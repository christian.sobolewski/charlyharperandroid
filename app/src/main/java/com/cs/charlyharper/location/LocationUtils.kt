package com.cs.charlyharper.location

import android.location.Address
import android.location.Geocoder
import android.location.Location
import com.cs.charlyharper.core.CoreCtrl
import java.util.*

object LocationUtils {

    fun getLocality(location : Location) : String {
        val geoInstance = Geocoder(CoreCtrl.mainActivity.applicationContext, Locale.getDefault())
        val addresses: List<Address>  = geoInstance.getFromLocation(location.latitude, location.longitude, 1)
        if (addresses.isNotEmpty()) {
            return addresses[0].locality
        }
        return ""
    }
}