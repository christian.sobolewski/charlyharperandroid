package com.cs.charlyharper.location

import android.annotation.SuppressLint
import android.location.Location
import android.util.Log
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.location.LocationPostData
import com.cs.charlyharper.request.location.LocationPostUser
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LocationHandler {

    private val TAG = "LocationHandler"

    fun initLocationHandler(lastLocation: Location) {
        locationApiCall(lastLocation)
    }

    @SuppressLint("CheckResult")
    private fun locationApiCall(lastLocation: Location) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.userLocationApiCall().doLocation(
                LocationPostUser(LocationPostData(SharedPreferencesUtils.getID(), lastLocation.latitude, lastLocation.longitude, LocationUtils.getLocality(lastLocation)))
            )
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ locationResponse ->
                    Log.i(TAG, "locationResponse=$locationResponse")
                }, { error ->
                    RequestErrorUtils.processError(error)
                }
                )
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.error_no_internet))
        }
    }
}