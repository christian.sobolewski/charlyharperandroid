package com.cs.charlyharper.utils

import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.*
import java.io.File

object PicassoUtils {

    private val TAG = "PicassoUtils"

    fun loadImage(fileName: String, imageView: ImageView, transformation: Transformation, callback: () -> Unit) {
        val path = ImageUtils.getPathForPicasso(fileName)
        Picasso.get().load(File(path)).fit().centerCrop().rotate(0f).memoryPolicy(MemoryPolicy.NO_CACHE)
            .networkPolicy(
                NetworkPolicy.OFFLINE
            ).transform(transformation).into(imageView, object : Callback {
                override fun onSuccess() {
                    //set animations here
                    Log.i(TAG, "Image file $path loaded")
                    callback()
                }

                override fun onError(e: Exception?) {
                    //do smth when there is picture loading error
                    if (e != null) {
                        Log.d(TAG, "" + e.stackTrace)
                    }
                }
            })
    }


}