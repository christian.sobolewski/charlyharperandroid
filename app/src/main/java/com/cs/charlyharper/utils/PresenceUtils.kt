package com.cs.charlyharper.utils

import java.util.*
import java.util.concurrent.TimeUnit

object PresenceUtils {

    fun getPresence(timestamp: Date?) : Int {
        if (timestamp != null) {
            val userTimestamp = timestamp.time
            val currentTimestamp = System.currentTimeMillis()
            val milliseconds = currentTimestamp - userTimestamp
            val hours = TimeUnit.MILLISECONDS.toHours(milliseconds)

            return when {
                hours < 3 ->
                    android.R.drawable.presence_online
                hours in 4..23 ->
                    android.R.drawable.presence_away
                else ->
                    android.R.drawable.presence_offline
            }
        }
        return android.R.drawable.presence_offline
    }
}