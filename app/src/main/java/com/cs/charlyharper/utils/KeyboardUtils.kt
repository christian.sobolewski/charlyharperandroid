package com.cs.charlyharper.utils

import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat

object KeyboardUtils {

    fun hideKeyboard(view: View) {
        if (!view.hasFocus())
        {
            val inputMethodManager =
                ContextCompat.getSystemService(view.context, InputMethodManager::class.java)!!
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}