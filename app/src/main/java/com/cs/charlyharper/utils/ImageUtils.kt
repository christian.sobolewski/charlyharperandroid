package com.cs.charlyharper.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.accountimages.AccountImageDTO
import okhttp3.MediaType
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.util.*


object ImageUtils {

    private val TAG = "ImageUtils"

    fun persistImage(bitmap: Bitmap, name: String) {
        val imageFile = File(getDir(), "$name.jpg")
        if (imageFile.exists()) imageFile.delete()
        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, os)
            os.flush()
            os.close()
        } catch (e: Exception) {
            Log.e(javaClass.simpleName, "Error writing bitmap", e)
        }
    }

    fun mimeTypeFromFile(file: File): MediaType? {
        return MediaType.parse("image/*")
    }

    fun getBitmapFromUri(imageUri: Uri): Bitmap? {
        if (android.os.Build.VERSION.SDK_INT >= 29) {
            // To handle deprication use
            val imageSrc =
                imageUri?.let {
                    ImageDecoder.createSource(CoreCtrl.mainActivity.contentResolver,
                        it
                    )
                }
            if (imageSrc != null) {
                return ImageDecoder.decodeBitmap(imageSrc)
            }
        } else {
            // Use older version
            return MediaStore.Images.Media.getBitmap(CoreCtrl.mainActivity.contentResolver, imageUri)
        }
        return null
    }

    fun copyStreamToFile(inputStream: InputStream, outputFile: File, callback: () -> Unit) {
        inputStream.use { input ->
            val outputStream = FileOutputStream(outputFile)
            outputStream.use { output ->
                val buffer = ByteArray(4 * 1024) // buffer size
                while (true) {
                    val byteCount = input.read(buffer)
                    if (byteCount < 0) break
                    output.write(buffer, 0, byteCount)
                }
                output.flush()
                callback.invoke()
            }
        }
    }

    fun getResultFile(dto: AccountImageDTO): File {
        return File(getDir(), getFileName(dto))
    }

    fun getFileName(dto: AccountImageDTO): String {
        return "${dto.id}_${dto.slotId}_${dto.imageToLoad}.jpg"
    }

    fun fileExistsFor(dto : AccountImageDTO) : Boolean {
        listDeviceImages()
        val fileCreatedAt = File(getDir(), getCreateAt(dto))

        if (dto.imageCreatedAt.isEmpty() || dto.imageUpdatedAt.isEmpty()) return false

        val old = Calendar.getInstance(Locale.getDefault())
        old.timeInMillis = dto.imageCreatedAt.toLong()
        val new = Calendar.getInstance(Locale.getDefault())
        new.timeInMillis = dto.imageUpdatedAt.toLong()
        val needsFileUpdate = new.compareTo(old)
        if (needsFileUpdate > 0) {
            dto.imageToLoad = dto.imageUpdatedAt
            if (fileCreatedAt.exists()) {
                removeFileFromDevice(getCreateAt(dto))
            }
        } else {
            dto.imageToLoad = dto.imageCreatedAt
        }
        val resultFile = File(getDir(), getFileName(dto))
        return resultFile.exists()
    }

    private fun getCreateAt(dto: AccountImageDTO): String {
        return "${dto.id}_${dto.slotId}_${dto.imageCreatedAt}.jpg"
    }

    fun getUpdatedAt(dto: AccountImageDTO): String {
        return "${dto.id}_${dto.slotId}_${dto.imageUpdatedAt}.jpg"
    }

    private fun getDir():File {return CoreCtrl.mainActivity.applicationContext.getDir(SharedPreferencesUtils.getID(), Context.MODE_PRIVATE)}

    fun getPathForPicasso(fileName : String): String {
        val resultFile = File(getDir(), fileName)
        return resultFile.path
    }

    fun removeFileFromDevice(fileName: String) {
        val imageFile = File(getDir(), fileName)
        try {
            imageFile.delete()
        } catch (e: KotlinNullPointerException)
        {
            Log.i(TAG, e.message)
        }
        listDeviceImages()
    }

    fun renameFile(old: String, new:String):Boolean {
        val oldimage = File(getDir(), old)
        val newimage = File(getDir(), new)
        var success = false
        if (oldimage.exists()) {
            success = oldimage.renameTo(newimage)
        }
        Log.d(TAG, "oldfile? ${oldimage.exists()}renameFile: ${oldimage.name} to ${newimage.name} == $success")
        return success
    }

    fun listDeviceImages() {
        val files: Array<File> = getDir().listFiles()
        Log.d(TAG, "Size: " + files.size)
        for (i in files.indices) {
            Log.d(TAG, "FileName:: " + files[i].name)
        }
    }
}