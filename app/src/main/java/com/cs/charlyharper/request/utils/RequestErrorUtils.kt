package com.cs.charlyharper.request.utils

import com.cs.charlyharper.core.CoreCtrl
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.HttpException

object RequestErrorUtils {

    fun processError(error: Any?) {
        if (error is HttpException) {
            val httpException: HttpException = error
            val errorBody: ResponseBody? = httpException.response().errorBody()
            if (errorBody != null) {
                val rawJsonString = errorBody.string()
                if (rawJsonString.contains("<!DOCTYPE html>")) {
                    return UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, "Internal server error")
                }
                val data: JsonObject = Gson().fromJson(rawJsonString, JsonObject::class.java)
                val errorJson = data.get("error").asJsonObject
                if (errorJson != null) {
                    val message = errorJson.get("msg").asString
                    UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, message.toString())
                } else {
                    UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, error.message.toString())
                }
            } else {
                UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, error.message.toString())
            }
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, "Request Error:: ${error.toString()}")
        }
    }
}