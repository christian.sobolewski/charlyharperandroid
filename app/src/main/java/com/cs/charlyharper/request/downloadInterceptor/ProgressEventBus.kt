package com.cs.charlyharper.request.downloadInterceptor

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class ProgressEventBus {

    private val mBusSubject: PublishSubject<ProgressEvent> = PublishSubject.create()

    fun post(progressEvent: ProgressEvent) {
        mBusSubject.onNext(progressEvent)
    }

    fun observable(): Observable<ProgressEvent> = mBusSubject
}