package com.cs.charlyharper.request.nexthits

import android.annotation.SuppressLint
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.general.PostId
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NextHitsApiCall {

    private val TAG = "NextHitsApiCall"

    @SuppressLint("CheckResult")
    fun callHitApi(hitId:String, callback: (NextHitApiResponse) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val hitPostUser = HitPostUser(PostId(SharedPreferencesUtils.getID()!!), PostId(hitId))
            val observable = ApiService.nextHitsApiCall().doNextHit(hitPostUser)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ nextHitResponse ->
                    callback(nextHitResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun callDisHitApi(disHitId:String, callback: (NextDisHitApiResponse) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val diHitPostUser = HitPostUser(PostId(SharedPreferencesUtils.getID()!!), PostId(disHitId))
            val observable = ApiService.nextHitsApiCall().doDisNextUser(diHitPostUser)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ nextHitResponse ->
                    callback(nextHitResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }
}
