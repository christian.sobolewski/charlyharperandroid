package com.cs.charlyharper.request.accountImage

import com.cs.charlyharper.request.general.GeneralApiResponse
import com.cs.charlyharper.request.downloadInterceptor.DOWNLOAD_IDENTIFIER_HEADER
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import okhttp3.ResponseBody
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface AccountImageApiService
{
    @Headers("Accept:*/*", "Content-Type:application/octet-stream","Connection:keep-open")
    @GET("user/images/accountImage")
    @Streaming
    fun doAccountImage(@Query("id") id: String?, @Query("slotId") slotId: String?, @Header(
        DOWNLOAD_IDENTIFIER_HEADER
    ) identifier: String): Observable<Response<ResponseBody>>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @DELETE("user/images/deleteAccountImage")
    fun doDeleteAccountImage(@Query("id") id: String?, @Query("slotId") slotId: String?): Observable<GeneralApiResponse>

    @Multipart
    @POST("user/images/uploadAccountImage")
    fun uploadFile(@Query("id") id: String?,@Query("slotId") slotId: String?, @Part accountImage: MultipartBody.Part): Observable<UploadAccountImageApiResponse>
}

data class UploadAccountImageApiResponse(
    @SerializedName("success") var success : Boolean,
    @SerializedName("id") var id : String,
    @SerializedName("slotId") var slotId : String,
    @SerializedName("accountImageCreatedAt") var accountImageCreatedAt : String,
    @SerializedName("accountImageUpdatedAt") var accountImageUpdatedAt : String
)