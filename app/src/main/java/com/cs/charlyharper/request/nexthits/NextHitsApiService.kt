package com.cs.charlyharper.request.nexthits

import com.cs.charlyharper.request.general.PostId
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.http.*

interface NextHitsApiService
{
    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @GET("user/location/userLocation")
    fun doNextHits(@Query("id") id: String?): Observable<List<NextHitsApiResponse>>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/matches/userMatch")
    fun doNextHit(@Body hitPostData: HitPostUser): Observable<NextHitApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/matches/userDismatch")
    fun doDisNextUser(@Body hitPostData: HitPostUser): Observable<NextDisHitApiResponse>
}

data class HitPostUser (
    @SerializedName("user") var user : PostId,
    @SerializedName("userMatch") var userMatch : PostId
)

data class NextHitApiResponse (
    @SerializedName("hit") var hit : Boolean,
    @SerializedName("hitAllowed") var hitAllowed : Boolean,
    @SerializedName("timer") var timer : Long = 0
)


data class NextDisHitApiResponse (
    @SerializedName("success") var success : Boolean,
    @SerializedName("possibleId") var possibleId : String = ""
)


