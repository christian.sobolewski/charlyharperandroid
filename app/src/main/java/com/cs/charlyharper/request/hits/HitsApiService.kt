package com.cs.charlyharper.request.hits

import com.cs.charlyharper.request.general.GeneralApiResponse
import com.cs.charlyharper.request.nexthits.NextHitsApiResponse
import io.reactivex.Observable
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface HitsApiService
{
    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @GET("user/matches/userMatches")
    fun doHits(@Query("id") id: String?): Observable<List<NextHitsApiResponse>>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @DELETE("user/matches/deleteHit")
    fun doDeleteHit(@Query("id") id: String?, @Query("userMatchId") userMatchId: String?): Observable<GeneralApiResponse>
}

