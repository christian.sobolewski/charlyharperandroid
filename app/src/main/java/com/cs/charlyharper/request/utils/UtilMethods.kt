package com.cs.charlyharper.request.utils

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl


object UtilMethods {

    private lateinit var progressDialogBuilder: AlertDialog.Builder
    private lateinit var progressDialog: AlertDialog

    private const val TAG: String = "---UtilMethods"

    /**
     * @param context
     * @action show progress loader
     */
    fun showLoading(context: Context, msg : String){
        progressDialogBuilder = AlertDialog.Builder(context)
        progressDialogBuilder.setCancelable(false)
        progressDialogBuilder.setView(R.layout.layout_loading_dialog)

        progressDialog = progressDialogBuilder.create()
        progressDialog.show()
        val loadingTextView : TextView = progressDialog.findViewById(R.id.loadingTextview)
        loadingTextView.text = msg
    }

    /**
     * @action hide progress loader
     */
    fun hideLoading(){
        try {
            progressDialog.dismiss()
        }catch (ex: java.lang.Exception){
            Log.e(TAG, ex.toString())
        }
    }


    /**
     * @param context
     * @action show Long toast message
     */
    fun showLongToast(context: Context, message: String) {
        toast(context, message, R.layout.custom_toast)
    }

    fun showLongToastError(context: Context, message: String) {
        toast(context, message, R.layout.custom_error_toast)
    }

    private fun toast(context: Context, message: String, resource: Int) {
        val inflater = CoreCtrl.mainActivity.layoutInflater
        val layout = inflater.inflate(resource, null)
        val textView:TextView = layout.findViewById(R.id.text)
        textView.text = message
        val toast:Toast = Toast.makeText(context, message, Toast.LENGTH_LONG)
        toast.view = layout
        toast.show()
    }

    fun isConnectedToInternet(): Boolean {
        return CoreCtrl.hasNetwork
    }

}
