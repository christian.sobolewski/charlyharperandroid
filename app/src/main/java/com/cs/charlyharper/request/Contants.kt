package com.cs.charlyharper.request

object Constants {
    const val API_CONTENT_TYPE = "application/json; charset=utf-8"
    const val API_BASE_PATH = "http://192.168.0.178:3000/api/"
    const val API_AUTHORIZATION_KEY = "[your authentication key]"
}