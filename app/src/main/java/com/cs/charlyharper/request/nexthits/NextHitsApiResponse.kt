package com.cs.charlyharper.request.nexthits

import com.google.gson.annotations.SerializedName
import java.util.*

data class NextHitsApiResponse (
    @SerializedName("user") val user: NextHitsResponse
)

data class NextHitsResponse (
    @SerializedName("id") val id: String = "",
    @SerializedName("username") val username: String = "",
    @SerializedName("gender") val gender: Int = -1,
    @SerializedName("age") val age: Int = -1,
    @SerializedName("bio") val bio: String = "",
    @SerializedName("locality") val locality: String = "",
    @SerializedName("lastLogin") val lastLogin: Date? = null,
    @SerializedName("accountImages") val accountImages: List<NextHitsImage>
)

data class NextHitsImage(
    @SerializedName("_id") val id: String,
    @SerializedName("filename") val filename: String,
    @SerializedName("accountImageCreatedAt") val accountImageCreatedAt: String,
    @SerializedName("accountImageUpdatedAt") val accountImageUpdatedAt: String
)
