package com.cs.charlyharper.request.messages

import android.annotation.SuppressLint
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.general.PostId
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MessageApiCalls {

    @SuppressLint("CheckResult")
    fun callMessageSendApi(id:String, msg:String, callback: (MessageSendApiResponse) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.messagesApiCall().doSendMessage(BodyConversation(MessageConversation(
                PostId(SharedPreferencesUtils.getID()!!), PostId(id),PostMsg(msg)
            )))

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sendMessageResponse ->
                    callback(sendMessageResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                }
                )
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun callMessageDeleteApi(id:String, callback: (Boolean) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.messagesApiCall().doDeleteMessage(
                SharedPreferencesUtils.getID()!!, id)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sendMessageResponse ->
                    callback(sendMessageResponse.success)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun callMarkConversationApi(id:String, callback: (MessageSendApiResponse) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.messagesApiCall().doMarkConversation(MarkConversation(MarkUser(SharedPreferencesUtils.getID()!!, id)))

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ sendMessageResponse ->
                    callback(sendMessageResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun resolveHit(id:String, callback: () -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.hitsApiCall().doDeleteHit(SharedPreferencesUtils.getID()!!, id)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback()
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun reportHit(id:String, callback: () -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.messagesApiCall().doReportAndDismiss(SharedPreferencesUtils.getID()!!, id)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    callback()
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }
}