package com.cs.charlyharper.request.location

import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LocationApiService {

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/location/locationUser")
    fun doLocation(@Body locationPostUser: LocationPostUser): Observable<LocationApiResponse> // body data
}

data class LocationPostUser(
    @SerializedName("user") var user: LocationPostData
)

data class LocationPostData (
    @SerializedName("id") var id: String?,
    @SerializedName("latitude") var latitude: Double,
    @SerializedName("longitude") var longitude: Double,
    @SerializedName("locality") var locality: String
)