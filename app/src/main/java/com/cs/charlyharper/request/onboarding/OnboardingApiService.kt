package com.cs.charlyharper.request.onboarding

import com.cs.charlyharper.request.general.GeneralApiResponse
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.http.*

interface OnboardingApiService {

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/login/loginUser")
    fun doLogin(@Body loginPostData: LoginPostUser): Observable<OnboardingApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @GET("user/login/autoLogin")
    fun doAutoLogin(@Query("id") id: String?): Observable<OnboardingApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/register/registerUser")
    fun doRegister(@Body registerPostData: RegisterPostUser): Observable<OnboardingApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/update/updateProfile")
    fun doUpdateBio(@Body bioPostUser: BioPostUser): Observable<OnboardingApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @DELETE("user/delete/deleteUser")
    fun doDeleteAccount(@Query("id") id: String): Observable<GeneralApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/password/forgotPassword")
    fun doPwForget(@Body pwPostData: PWPostUser): Observable<PWApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/password/recoverPassword")
    fun doPwRecover(@Body recoverPwPostData: RecoverPwPostUser): Observable<OnboardingApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/update/updateFirebaseToken")
    fun doFirebaseToken(@Body firebaseTokenPostData: FirebaseTokenPostUser): Observable<GeneralApiResponse>

}

data class LoginPostUser(
    @SerializedName("user") var user : LoginPostData
)

data class LoginPostData(
    @SerializedName("email") var userEmail: String,
    @SerializedName("password") var userPassword: String
)

data class RegisterPostUser(
    @SerializedName("user") var user : RegisterPostData
)

data class RegisterPostData(
    @SerializedName("email") var userEmail: String,
    @SerializedName("password") var userPassword: String,
    @SerializedName("username") var username: String,
    @SerializedName("gender") var gender: Int,
    @SerializedName("birthdate") var birthdate: String
)

data class BioPostUser(
    @SerializedName("user") var user: BioPostData
)

data class BioPostData (
    @SerializedName("id") var id: String?,
    @SerializedName("bio") var bio: String
)

data class PWPostUser(
    @SerializedName("user") var user : PWPostData
)

data class PWPostData(
    @SerializedName("email") var email: String
)

data class PWApiResponse(
    @SerializedName("toast") var toast: String
)

data class RecoverPwPostUser(
    @SerializedName("user") var user : RecoverPwPostData
)

data class RecoverPwPostData (
    @SerializedName("token") var token: String,
    @SerializedName("password") var password: String
)

data class FirebaseTokenPostUser(
    @SerializedName("user") var user : FirebaseTokenPostData
)

data class FirebaseTokenPostData (
    @SerializedName("id") var id: String,
    @SerializedName("deviceToken") var deviceToken: String
)

