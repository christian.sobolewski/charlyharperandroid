package com.cs.charlyharper.request.accountImage

object AccountImageConstants {
    const val ACCOUNT_IMAGE_ID_0 = "0" // as Profile Image
    const val ACCOUNT_IMAGE_ID_1 = "1"
    const val ACCOUNT_IMAGE_ID_2 = "2"
    const val ACCOUNT_IMAGE_ID_3 = "3"
    const val ACCOUNT_IMAGE_ID_4 = "4"
}