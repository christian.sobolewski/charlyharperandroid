package com.cs.charlyharper.request.onboarding

import com.cs.charlyharper.request.filter.FilterValues
import com.google.gson.annotations.SerializedName

data class OnboardingApiResponse(
    @SerializedName("user") val user: OnboardingResponse
)

data class OnboardingResponse(
    @SerializedName("id") val id: String = "",
    @SerializedName("email") val email: String = "",
    @SerializedName("token") val token: String = "",
    @SerializedName("username") val username: String = "",
    @SerializedName("gender") val gender: Int = -1,
    @SerializedName("age") val age: Int = -1,
    @SerializedName("bio") val bio: String = "",
    @SerializedName("accountType") val accountType: Int,
    @SerializedName("filter") val filter: FilterValues,
    @SerializedName("accountImageCreatedAt") val accountImageCreatedAt: String = "",
    @SerializedName("accountImageUpdatedAt") val accountImageUpdatedAt: String = "",
    @SerializedName("unreadMessages") val unreadMessages: Int = 0
)

