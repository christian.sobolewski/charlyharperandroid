package com.cs.charlyharper.request.filter

import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.http.*

interface FilterApiService {

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @GET("user/filter/filter")
    fun doFilter(@Query("id") id: String?): Observable<FilterData>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/filter/filter")
    fun doPostFilter(@Body filterPost: FilterData): Observable<FilterData> // body data
}

data class FilterData(
    @SerializedName("filter") var filter: FilterValues
)

data class FilterValues (
    @SerializedName("id") var id: String?,
    @SerializedName("distanceLimit") var distanceLimit: Int?,
    @SerializedName("showGender") var showGender: Int?,
    @SerializedName("ageRange") var ageRange: ArrayList<Number>
)