package com.cs.charlyharper.request.messages

import com.cs.charlyharper.request.general.GeneralApiResponse
import com.cs.charlyharper.request.general.PostId
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.http.*

interface MessagesApiService
{
    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @GET("user/messages/messagesUser")
    fun doMessages(@Query("id") id: String?): Observable<MessagesApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/messages/messageMatch")
    fun doSendMessage(@Body conversation: BodyConversation): Observable<MessageSendApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @DELETE("user/messages/deleteMessage")
    fun doDeleteMessage(@Query("id") id: String?, @Query("userMatchId") userMatchId: String?): Observable<GeneralApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @POST("user/messages/markConversation")
    fun doMarkConversation(@Body conversation: MarkConversation): Observable<MessageSendApiResponse>

    @Headers("Accept:*/*", "Content-Type:application/json","Accept-Encoding:gzip, deflate, br","Connection:close")
    @DELETE("user/messages/report")
    fun doReportAndDismiss(@Query("id") id: String?, @Query("userMatchId") userMatchId: String?): Observable<GeneralApiResponse>
}

data class BodyConversation(
    @SerializedName("conversation") val conversation: MessageConversation
)

data class MessageConversation(
    @SerializedName("user") val user: PostId,
    @SerializedName("userMatch") val userMatch: PostId,
    @SerializedName("message") var message : PostMsg
)

data class PostMsg(
    @SerializedName("msg") var msg : String
)

data class MarkConversation(
    @SerializedName("user") var user : MarkUser
)

data class MarkUser(
    @SerializedName("id") var id : String,
    @SerializedName("senderId") var senderId : String
)