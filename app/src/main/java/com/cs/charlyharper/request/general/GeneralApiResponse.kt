package com.cs.charlyharper.request.general

import com.google.gson.annotations.SerializedName

data class GeneralApiResponse(
    @SerializedName("success") val success: Boolean
)

data class PostId(
    @SerializedName("id") var id : String
)
