package com.cs.charlyharper.request.accountImage

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.CoreEvents
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.accountimages.AccountImageFactoryDTO
import com.cs.charlyharper.ui.selectImage.SelectImage
import com.cs.charlyharper.utils.ImageUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class UploadAccountImage {

    private val TAG = "UploadAccountImage"

    fun init(data: Any?) {
        uploadAccountImage(data)
    }

    @SuppressLint("CheckResult")
    private fun uploadAccountImage(dataBitmap: Any?) {
        if (UtilMethods.isConnectedToInternet()) {
            UtilMethods.showLoading(CoreCtrl.mainActivity, "Please wait while Alan uploads your photo")
            val slotQuery = SelectImage.slotQuery
            val fileName = "${SharedPreferencesUtils.getID()}_slot_${slotQuery}"
            fileName.let { ImageUtils.persistImage(dataBitmap as Bitmap, it) }

            val directory = CoreCtrl.mainActivity.applicationContext.getDir(SharedPreferencesUtils.getID(), Context.MODE_PRIVATE)
            val imageFile = File(directory, "$fileName.jpg")

            val contentType = ImageUtils.mimeTypeFromFile(imageFile)
            val fileBody: RequestBody = RequestBody.create(contentType, imageFile)
            val tempBody = MultipartBody.Part.createFormData("accountImage", fileName, fileBody)

            val observable = ApiService.accountImageApiCall().uploadFile(SharedPreferencesUtils.getID(), slotQuery, tempBody)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {response: UploadAccountImageApiResponse ->
                        UtilMethods.hideLoading()
                        if (response.success) {
                            val dto = AccountImageFactoryDTO.factory(
                                response.id,
                                response.slotId,
                                response.accountImageCreatedAt,
                                response.accountImageUpdatedAt
                            )
                            ImageUtils.renameFile(imageFile.name, ImageUtils.getUpdatedAt(dto))
                            ImageUtils.removeFileFromDevice(fileName)
                            ImageUtils.listDeviceImages()
                            ImageUtils.fileExistsFor(dto)
                            if (SelectImage.hasProfileImagesModel()) {
                                SelectImage.accountImagesModel.exists = true
                                SelectImage.accountImagesModel.fileName = ImageUtils.getFileName(dto)
                                SelectImage.accountImagesModel.accountImageViewModel.onUpdate(SelectImage.accountImagesModel, dto)
                            } else {
                                if (response.accountImageCreatedAt.isNotEmpty() && response.accountImageUpdatedAt.isNotEmpty()) {
                                    CoreCtrl.hasAccountImage = true
                                }
                                CoreCtrl.handleUI(CoreEvents.UPDATE_PROFILE_IMAGE, dto)
                            }
                        }
                    }
                ) { error ->
                    UtilMethods.hideLoading()
                    RequestErrorUtils.processError(error)
                }

        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }
}
