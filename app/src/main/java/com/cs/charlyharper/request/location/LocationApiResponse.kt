package com.cs.charlyharper.request.location

import com.google.gson.annotations.SerializedName

data class LocationApiResponse (
    @SerializedName("success") val success: Boolean
)