package com.cs.charlyharper.request.messages

import com.google.gson.annotations.SerializedName
import java.util.*

data class MessagesApiResponse(
    @SerializedName("user") val user: MessagesUserResponse
)

data class MessagesUserResponse (
    @SerializedName("id") val id: String = "",
    @SerializedName("messages") val messages: ArrayList<MessagesResponse>
)

data class MessagesResponse (
    @SerializedName("username") val username: String = "",
    @SerializedName("id") val id: String = "",
    @SerializedName("accountImageCreatedAt") val accountImageCreatedAt: String = "",
    @SerializedName("accountImageUpdatedAt") val accountImageUpdatedAt: String = "",
    @SerializedName("latestMsgTimestamp") val latestMsgTimestamp: String = "",
    @SerializedName("lastLogin") val lastLogin: Date? = null,
    @SerializedName("conversations") var conversations: ArrayList<ConversationsResponse>
)

data class ConversationsResponse (
    @SerializedName("timestamp") val timestamp: String,
    @SerializedName("message") val message: String,
    @SerializedName("senderId") val senderId: String,
    @SerializedName("messageRead") val messageRead: Boolean?
)

data class MessageSendApiResponse(
    @SerializedName("conversation") val conversation: MessagesResponse
)
