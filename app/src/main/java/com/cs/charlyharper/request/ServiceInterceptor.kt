package com.cs.charlyharper.request

import android.os.Build
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import okhttp3.Interceptor
import okhttp3.Response

class ServiceInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var original = chain.request()

        if(original.header("No-Authentication") == null){
            //or use Token Function
            if (SharedPreferencesUtils.getToken() != null) {
                original = original.newBuilder()
                    .addHeader("Accept", "*/*")
                    .addHeader("Authorization", SharedPreferencesUtils.getToken()!!)
                    .addHeader("Client", "android")
                    .build()
                return chain.proceed(original)
            }
        }
        return chain.proceed(original)
    }
}