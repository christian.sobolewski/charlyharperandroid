package com.cs.charlyharper.request.downloadInterceptor

interface DownloadProgressListener {
    fun update(downloadIdentifier: String, bytesRead: Long, contentLength: Long, done: Boolean)
}