package com.cs.charlyharper.request.onboarding

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.CoreEvents
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.general.GeneralApiResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

const val TYPE_LOGIN: Int = 0
const val TYPE_REGISTER: Int = 1

class OnboardingApiCall {

    private val TAG = "OnboardingApiCall"


    @SuppressLint("CheckResult")
    fun loginApiCall(userEmail: String, userPassword: String) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doLogin(
                LoginPostUser(LoginPostData(userEmail, userPassword))
            )
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ loginResponse ->
                    handleResult(loginResponse, TYPE_LOGIN)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun autoLoginApiCall() {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doAutoLogin(SharedPreferencesUtils.getID())
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ loginResponse ->
                    handleResult(loginResponse, TYPE_LOGIN)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun registerApiCall(userEmail: String, userPassword: String, userName: String, gender: Int, birthdate: String) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doRegister(
                RegisterPostUser(RegisterPostData(userEmail, userPassword, userName, gender, birthdate))
            )
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ registerResponse ->
                    handleResult(registerResponse, TYPE_REGISTER)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.error_no_internet))
        }
    }

    private fun handleResult(response: OnboardingApiResponse, apiType: Int) {
        SharedPreferencesUtils.saveToken(response.user.token)
        SharedPreferencesUtils.saveID(response.user.id)
        SharedPreferencesUtils.saveAccountType(response.user.accountType)
        if (response.user.accountImageCreatedAt.isNotEmpty() && response.user.accountImageUpdatedAt.isNotEmpty()) {
            CoreCtrl.hasAccountImage = true
        }
        CoreCtrl.handleUI(CoreEvents.LOGIN, response)
        // Firebase Analytics Log Event based on apiType
        var evt = ""
        val bundle = Bundle()
        when (apiType) {
            TYPE_LOGIN -> {
                evt = FirebaseAnalytics.Event.LOGIN
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, response.user.username)
            }
            TYPE_REGISTER -> {
                evt = FirebaseAnalytics.Event.SIGN_UP
                bundle.putString(FirebaseAnalytics.UserProperty.SIGN_UP_METHOD, "E-Mail")
            }
        }
        if (evt.isNotEmpty()) CoreCtrl.firebaseAnalytics.logEvent(evt, bundle)
    }

    @SuppressLint("CheckResult")
    fun bioApiCall(bio: String, callback: (OnboardingApiResponse) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doUpdateBio(
                BioPostUser(BioPostData(SharedPreferencesUtils.getID(), bio))
            )
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ bioResponse ->
                    callback(bioResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun deleteAccountApiCall(callback: (GeneralApiResponse) -> Unit) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doDeleteAccount(SharedPreferencesUtils.getID()!!)
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    callback(response)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun pwForgetApiCall(userEmail: String) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doPwForget(
                PWPostUser(PWPostData(userEmail))
            )
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pwResponse ->
                    UtilMethods.showLongToast(CoreCtrl.mainActivity.applicationContext, pwResponse.toast)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun pwRecoverApiCall(pw: String, token: String) {
        if(UtilMethods.isConnectedToInternet()){
            val observable = ApiService.onboardingApiCall().doPwRecover(
                RecoverPwPostUser(RecoverPwPostData(token, pw))
            )
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pwResponse ->
                    handleResult(pwResponse, TYPE_LOGIN)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }
}