package com.cs.charlyharper.sharedPreferences

import android.content.Context
import android.content.SharedPreferences
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.cs.charlyharper.location.ForegroundOnlyLocationService


object SharedPreferencesUtils {

    private const val TOKEN = "Token"
    private const val ID = "id"
    private const val ACCOUNT_TYPE = "account_type"

    private lateinit var sharedPreferences: SharedPreferences

    fun init(context: Context) {

        val spec = KeyGenParameterSpec.Builder(
            "User",
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        )
            .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
            .setKeySize(256)
            .build()

        val masterKeyAlias = MasterKey.Builder(context, "User")
            .setKeyGenParameterSpec(spec)
            .build()
        sharedPreferences = EncryptedSharedPreferences.create(
            context,
            "charly_harper_user",
            masterKeyAlias,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM);
    }

    fun saveToken(token: String?) {
        sharedPreferences.edit()
            .putString(TOKEN, "Token $token")
            .apply()
    }

    fun getToken(): String? {return sharedPreferences.getString(TOKEN, "")}

    fun saveID(id: String?) {
        sharedPreferences.edit()
            .putString(ID, id)
            .apply()
    }

    fun getID(): String? {return sharedPreferences.getString(ID, "")}

    fun getLocationTrackingPref(foregroundOnlyLocationService: ForegroundOnlyLocationService): Boolean {
        return sharedPreferences.getBoolean(foregroundOnlyLocationService.toString(), true)
    }

    fun saveLocationTrackingPref(foregroundOnlyLocationService: ForegroundOnlyLocationService, b: Boolean) {
        sharedPreferences.edit().putBoolean(foregroundOnlyLocationService.toString(), b).apply()
    }

    fun saveAccountType(accountType: Int) {
        sharedPreferences.edit()
            .putInt(ACCOUNT_TYPE, accountType)
            .apply()
    }

    fun getAccountType(): Int? {return sharedPreferences.getInt(ACCOUNT_TYPE, 0)}

    fun deleteToken() {
        sharedPreferences.edit()
            .remove(TOKEN)
            .apply()
    }

    fun deleteId() {
        sharedPreferences.edit()
            .remove(ID)
            .apply()
    }


}