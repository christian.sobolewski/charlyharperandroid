package com.cs.charlyharper.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import com.cs.charlyharper.core.CoreCtrl

class NetworkDectection {

   //private val TAG: String = "---NetworkDectection"
    private lateinit var connectivityManager: ConnectivityManager

    fun initialize(coreCtrl: CoreCtrl) {

        connectivityManager = coreCtrl.mainActivity.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onLost(network: Network?) {
            }
            override fun onUnavailable() {
            }
            override fun onLosing(network: Network?, maxMsToLive: Int) {
            }
            override fun onAvailable(network: Network?) {
            }
        }
        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)
    }

    fun hasNetwork(): Boolean {
        return connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork) != null
    }
}