package com.cs.charlyharper.core.deleteAccount

import android.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils

object DeleteAccountCtrl {

    private const val TAG: String = "DeleteAccountCtrl"

    fun showDialog(context: MainActivity) {
        val builder = AlertDialog.Builder(context).create()
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_permissions, null)
        builder.setView(dialogView)

        val tfHead = dialogView.findViewById<TextView>(R.id.title)
        tfHead.text = context.resources.getString((R.string.action_delete_h1))

        val tfMsg = dialogView.findViewById<TextView>(R.id.message)
        tfMsg.text = context.resources.getString((R.string.action_delete_copy))

        val btnOk: Button = dialogView.findViewById(R.id.btn_ok)
        btnOk.setOnClickListener {
            builder.cancel()
            val api = OnboardingApiCall()
            api.deleteAccountApiCall(callback = { response ->
                if (response.success) {
                    SharedPreferencesUtils.deleteId()
                    SharedPreferencesUtils.deleteToken()
                    builder.cancel()
                    context.moveTaskToBack(true)
                    context.finishAffinity()
                } else Log.d(TAG, "Account could not be deleted")
            })

        }

        val btnCancel: Button = dialogView.findViewById(R.id.btn_cancel)
        btnCancel.setOnClickListener {
            builder.cancel()
        }

        builder.show()
    }

}