package com.cs.charlyharper.core.permission

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.view.LayoutInflater
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R

object PermissionCtrl {

    const val PERMISSION_CAM_AND_GALLERY: Int = 123

    private fun hasPermissions(
        context: Context?,
        vararg permissions: String?
    ): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        permission!!
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }


    fun init(context: Context, mainActivity: MainActivity) {
        val permissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.FOREGROUND_SERVICE
            )
        } else {
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }

        val neededPermissionsList: ArrayList<String> = ArrayList()

        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                neededPermissionsList.add(permission)
            }
        }
        val neededPermissions = neededPermissionsList.toTypedArray()

        val hasPerm = hasPermissions(context, *neededPermissions)

        if (!hasPerm) {
            ActivityCompat.requestPermissions(
                mainActivity,
                neededPermissions,
                PERMISSION_CAM_AND_GALLERY
            )
        } else {
            mainActivity.startApplication()
        }
    }

    fun showPermissionDialog(context: MainActivity) {
        val builder = AlertDialog.Builder(context).create()
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_permissions, null)
        builder.setView(dialogView)

        val btnOk: Button = dialogView.findViewById(R.id.btn_ok)
        btnOk.setOnClickListener {
            builder.cancel()
            context.startPermission()
        }

        val btnCancel: Button = dialogView.findViewById(R.id.btn_cancel)
        btnCancel.setOnClickListener {
            context.moveTaskToBack(true)
            context.finishAffinity()
        }

        builder.show()
    }

}