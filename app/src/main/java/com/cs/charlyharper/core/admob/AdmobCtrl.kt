package com.cs.charlyharper.core.admob

import android.os.Bundle
import android.util.Log
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.google.ads.consent.ConsentStatus
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds


object AdmobCtrl {

    private val TAG = "AdmobCtrl"

    private lateinit var mInterstitialAd: InterstitialAd
    private var maxUserActions = 10
    private var mUserAction: Int = 0

    init {
        MobileAds.initialize(CoreCtrl.mainActivity, CoreCtrl.mainActivity.resources.getString(R.string.ad_mob_application_id))
        mInterstitialAd = InterstitialAd(CoreCtrl.mainActivity)
        mInterstitialAd.adUnitId = CoreCtrl.mainActivity.resources.getString(R.string.ad_mob_interstitial_id)
    }

    fun initialize() {}

    fun updateCounting() {
        val showConsentForm = ConsentCtrl.showConsentForm(true)
        if (!showConsentForm) {
            mUserAction++
            val show = mUserAction % maxUserActions == 0
            if (show) {
                showInterstitialAd()
            }
        }
    }

    private fun showInterstitialAd() {
        if (mInterstitialAd.isLoaded) {
            mInterstitialAd.show()
            mUserAction = 0
        }
    }

    fun loadInterstitialAd(consentStatus: Enum<ConsentStatus>, showOnAdLoaded: Boolean = false) {
        mInterstitialAd.adListener = object: AdListener() {
            override fun onAdLoaded() {
                if (showOnAdLoaded) {
                    mInterstitialAd.show()
                    mUserAction = 0
                }
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                Log.i(TAG, "onAdFailedToLoad $errorCode")
                // Code to be executed when an ad request fails.
            }

            override fun onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            override fun onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                loadInterstitialAd(consentStatus)
            }
        }

        if (consentStatus == ConsentStatus.NON_PERSONALIZED) {
            val extras = Bundle()
            extras.putString("npa", "1")
            val request =
                AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter::class.java, extras)
                    .build()
            mInterstitialAd.loadAd(request)
        } else mInterstitialAd.loadAd(AdRequest.Builder().build())
    }
}