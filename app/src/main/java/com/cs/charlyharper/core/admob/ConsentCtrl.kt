package com.cs.charlyharper.core.admob

import android.util.Log
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.utils.UtilMethods
import com.google.ads.consent.*
import java.net.MalformedURLException
import java.net.URL

object ConsentCtrl {

    private const val TAG = "ConsentCtrl"

    private lateinit var consentForm: ConsentForm
    private lateinit var consentStatus: ConsentStatus
    private var mIsAdMob: Boolean = true

    init {
        val consentInformation = ConsentInformation.getInstance(CoreCtrl.mainActivity)
        val publisherIds =
            arrayOf(CoreCtrl.mainActivity.resources.getString(R.string.ad_mob_publisher_id))
        consentInformation.requestConsentInfoUpdate(
            publisherIds,
            object : ConsentInfoUpdateListener {
                override fun onConsentInfoUpdated(cs: ConsentStatus) {
                    // User's consent status successfully updated.
                    consentStatus = cs
                    // if user chooses non - personalized ads load add with extras
                    if (consentStatus == ConsentStatus.NON_PERSONALIZED) {
                        AdmobCtrl.loadInterstitialAd(consentStatus)
                    }
                }

                override fun onFailedToUpdateConsentInfo(errorDescription: String) {
                    // User's consent status failed to update.
                    Log.i(TAG, errorDescription)
                }
            })

        var privacyUrl: URL? = null
        try {
            privacyUrl = URL(CoreCtrl.mainActivity.resources.getString(R.string.privacy_url))
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        consentForm = ConsentForm.Builder(CoreCtrl.mainActivity, privacyUrl)
            .withListener(object : ConsentFormListener() {
                override fun onConsentFormLoaded() {
                    // Consent form loaded successfully.
                    consentForm.show()
                }

                override fun onConsentFormOpened() {
                    // Consent form was displayed.
                    Log.i(TAG, "onConsentFormOpened")
                }

                override fun onConsentFormClosed(
                    cs: ConsentStatus, userPrefersAdFree: Boolean
                ) {
                    // Consent form was closed.
                    if (mIsAdMob && consentStatus != cs) {
                        AdmobCtrl.loadInterstitialAd(cs, true)
                    } else AdmobCtrl.loadInterstitialAd(cs, false)

                    consentStatus = cs
                }

                override fun onConsentFormError(errorDescription: String) {
                    // Consent form error.
                    UtilMethods.showLongToastError(CoreCtrl.mainActivity, errorDescription)
                }
            })
            .withPersonalizedAdsOption()
            .withNonPersonalizedAdsOption()
            .build()
    }

    fun initialize() {}

    fun showConsentForm(isAdMob:Boolean ) : Boolean {
        mIsAdMob = isAdMob
        if (consentStatus == ConsentStatus.UNKNOWN && isAdMob)  {
            consentForm.load()
            return true
        } else if (!isAdMob) {
            consentForm.load()
            return true
        }
        return false
    }

}