package com.cs.charlyharper.core.pwForget

import android.app.AlertDialog
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.ui.onboarding.InputVerification

object PwForgetCtrl {

    private const val TAG: String = "PwForgetCtrl"

    fun showDialog(context: MainActivity) {
        val builder = AlertDialog.Builder(context).create()
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_pw, null)
        builder.setView(dialogView)

        val tfHead = dialogView.findViewById<TextView>(R.id.title)
        tfHead.text = context.resources.getString((R.string.pw_forget_h1))

        val tfMsg = dialogView.findViewById<TextView>(R.id.message)
        tfMsg.text = context.resources.getString((R.string.pw_forget_copy))

        val tfE = dialogView.findViewById<EditText>(R.id.emailInput)

        val btnOk: Button = dialogView.findViewById(R.id.btn_ok)
        btnOk.setOnClickListener {
            if (InputVerification.isValidEmail(tfE.text)) {
                builder.cancel()
                val api = OnboardingApiCall()
                api.pwForgetApiCall(tfE.text.toString())
            } else {
                tfE.hint = context.resources.getString(R.string.email_validation_error)
            }

        }

        val btnCancel: Button = dialogView.findViewById(R.id.btn_cancel)
        btnCancel.setOnClickListener {
            builder.cancel()
        }

        builder.show()
    }

}