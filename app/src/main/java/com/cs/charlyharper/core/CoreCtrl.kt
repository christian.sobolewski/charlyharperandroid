package com.cs.charlyharper.core

import android.location.Location
import android.net.Uri
import android.text.format.DateFormat
import android.view.View
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.firebase.FirebaseMessagingService
import com.cs.charlyharper.network.NetworkDectection
import com.cs.charlyharper.request.accountImage.UploadAccountImage
import com.cs.charlyharper.request.filter.FilterValues
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.request.onboarding.OnboardingApiResponse
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.accountimages.AccountImageDTO
import com.cs.charlyharper.ui.onboarding.OnboardingUI
import com.cs.charlyharper.ui.onboarding.PwUI
import com.cs.charlyharper.ui.onboarding.RegisterUI
import com.cs.charlyharper.ui.profileImage.ProfileImageUI
import com.cs.charlyharper.ui.selectImage.SelectImage
import com.cs.charlyharper.utils.ImageUtils
import com.google.firebase.analytics.FirebaseAnalytics
import java.text.SimpleDateFormat
import java.util.*


object CoreCtrl {

    private val TAG = "CoreCtrl"

    lateinit var lastUserLocation: Location
    lateinit var mainActivity: MainActivity
    lateinit var filterData: FilterValues
    lateinit var firebaseAnalytics: FirebaseAnalytics
    var hasAccountImage: Boolean = false
    var isLoggedIn : Boolean = false

    private var networkDetection: NetworkDectection = NetworkDectection()

    val hasNetwork: Boolean
        get() {return networkDetection.hasNetwork()}

    private lateinit var profileImageUI: ProfileImageUI

    fun handleUI(event: String, data:Any?) {
        when (event) {
            CoreEvents.START -> handleStart()
            CoreEvents.LOGIN -> handleLogin(data)
            CoreEvents.REGISTER -> handleRegister()
            CoreEvents.REGISTER_DONE -> handleRegisterdone(data)
            CoreEvents.REGISTER_UNDONE -> handleRegisterUndone()
            CoreEvents.CAMERA_PROFILE_IMAGE -> handleAccountImage(data)
            CoreEvents.GALLERY_PROFILE_IMAGE -> handleAccountImage(data)
            CoreEvents.UPDATE_PROFILE_IMAGE -> updateProfileImage(data)
            CoreEvents.CAMERA_ACCOUNT_IMAGE -> handleAccountImage(data)
            CoreEvents.GALLERY_ACCOUNT_IMAGE -> handleAccountImage(data)
            CoreEvents.PW_FORGET -> handlePW(data)
        }
    }

    fun initialize() {
        networkDetection.initialize(this)
    }

    private fun handleStart() {
        val token = SharedPreferencesUtils.getToken()
        if (token?.isNotEmpty()!!) {
            val apiCall = OnboardingApiCall()
            apiCall.autoLoginApiCall()
        } else {
            mainActivity.setContentView(R.layout.layout_onboarding)
            val onboardingUI = OnboardingUI()
            onboardingUI.init(mainActivity)
        }
    }

    private fun handleLogin(data: Any?) {
        isLoggedIn = true
        filterData = (data as OnboardingApiResponse).user.filter
        removeOnboardingLayout()
        removePWLayout()
        mainActivity.initMainUI(data)
        mainActivity.initLocation()
        profileImageUI = ProfileImageUI(data)
        FirebaseMessagingService.Companion.getToken()
    }

    private fun handleRegister() {
        removeOnboardingLayout();
        mainActivity.setContentView(R.layout.layout_register)
        val registerIU = RegisterUI()
        registerIU.init(mainActivity)
    }

    private fun handleRegisterdone(data: Any?) {
        removeRegisterLayout()
        handleLogin(data)
    }

    private fun handleRegisterUndone() {
        removeRegisterLayout()
        handleStart();
    }

    private fun handleAccountImage(data : Any?) {
        if (data is Uri) {
            uploadAccountImage(ImageUtils.getBitmapFromUri(data))
        } else {
            uploadAccountImage(ImageUtils.getBitmapFromUri(SelectImage.photoURI))
        }
    }

    private fun handlePW(data: Any?) {
        mainActivity.setContentView(R.layout.layout_pw)
        val pw = PwUI()
        pw.init(mainActivity, data)
    }

    private fun uploadAccountImage(data: Any?) {
        if (data != null) {
            val uploadAccountImage = UploadAccountImage()
            uploadAccountImage.init(data)
        }
    }

    private fun updateProfileImage(data: Any?) {
        profileImageUI.initProfileImage(data as AccountImageDTO)
    }

    private fun removeOnboardingLayout() {
        val onboarding: View? = mainActivity.findViewById(R.id.layout_onboarding)
        if (onboarding != null) onboarding.visibility = View.GONE
    }

    private fun removeRegisterLayout() {
        val register: View? = mainActivity.findViewById(R.id.layout_register)
        if (register != null) register.visibility = View.GONE;
    }

    private fun removePWLayout() {
        val pw: View? = mainActivity.findViewById(R.id.layout_pw)
        if (pw != null) pw.visibility = View.GONE;
    }

    fun getTime(timestamp: Long): String {
        val longDateFormat: SimpleDateFormat = DateFormat.getLongDateFormat(mainActivity) as SimpleDateFormat
        longDateFormat.calendar.timeInMillis = timestamp
        val timeFormat: SimpleDateFormat = DateFormat.getTimeFormat(mainActivity) as SimpleDateFormat
        timeFormat.calendar.timeInMillis = timestamp

        val calendar = Calendar.getInstance(Locale.getDefault())
        calendar.timeInMillis = timestamp

        longDateFormat.toLocalizedPattern()
        timeFormat.toLocalizedPattern()
        val pattern = longDateFormat.toLocalizedPattern() +", "+ timeFormat.toPattern()

        return DateFormat.format(pattern, calendar).toString()
    }

}