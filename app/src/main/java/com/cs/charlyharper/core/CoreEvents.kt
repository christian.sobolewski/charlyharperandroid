package com.cs.charlyharper.core

object CoreEvents {
    const val START = "START"
    const val LOGIN = "LOGIN"
    const val REGISTER = "REGISTER"
    const val REGISTER_DONE = "REGISTER_DONE"
    const val REGISTER_UNDONE = "REGISTER_UNDONE"
    const val PW_FORGET = "PW_FORGET"

    const val CAMERA_PROFILE_IMAGE = "CAMERA_PROFILE_IMAGE"
    const val GALLERY_PROFILE_IMAGE = "GALLERY_PROFILE_IMAGE"
    const val UPDATE_PROFILE_IMAGE = "UPDATE_PROFILE_IMAGE"

    const val CAMERA_ACCOUNT_IMAGE = "CAMERA_ACCOUNT_IMAGE"
    const val GALLERY_ACCOUNT_IMAGE = "GALLERY_ACCOUNT_IMAGE"

}