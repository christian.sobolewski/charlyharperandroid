package com.cs.charlyharper.core.legal

import android.app.AlertDialog
import android.os.Build
import android.text.Html
import android.text.Html.FROM_HTML_MODE_LEGACY
import android.text.Spanned
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R

object LegalCtrl {

    private const val TAG: String = "LicenceCtrl"

    fun showLicenceDialog(context: MainActivity) {
        val builder = AlertDialog.Builder(context).create()
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_licence, null)
        builder.setView(dialogView)

        val tfHead = dialogView.findViewById<TextView>(R.id.title)
        tfHead.text = context.resources.getString((R.string.action_licences))

        val tfMsg = dialogView.findViewById<TextView>(R.id.message)
        tfMsg.movementMethod = ScrollingMovementMethod();

        val array: Array<String> = context.resources.getStringArray(R.array.licences)
        var text = ""
        for (it in array) {
            text += it
        }

        val styledText: Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }

        tfMsg.text = styledText

        val btnOk: Button = dialogView.findViewById(R.id.btn_ok)
        btnOk.setOnClickListener {
            builder.cancel()
        }

        builder.show()
    }

}