package com.cs.charlyharper

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.*
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.view.get
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavHostController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.CoreEvents
import com.cs.charlyharper.core.admob.AdmobCtrl
import com.cs.charlyharper.core.admob.ConsentCtrl
import com.cs.charlyharper.core.deleteAccount.DeleteAccountCtrl
import com.cs.charlyharper.core.legal.LegalCtrl
import com.cs.charlyharper.core.permission.PermissionCtrl
import com.cs.charlyharper.location.ForegroundOnlyLocationService
import com.cs.charlyharper.request.onboarding.OnboardingApiResponse
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.IntentConstants
import com.cs.charlyharper.ui.header.NavigationHeaderUI
import com.cs.charlyharper.ui.indicator.IndicatorConstants
import com.cs.charlyharper.ui.indicator.IndicatorModel
import com.cs.charlyharper.ui.indicator.IndicatorViewModel
import com.google.android.material.navigation.NavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import java.util.*


class MainActivity : AppCompatActivity()   {

    private val TAG = "MainActivity"

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavHostController
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var indicatorViewModel: IndicatorViewModel
    private var msgDeeplinkId: Any? = ""

    private lateinit var messageMenuItem: MenuItem
    private lateinit var hitsMenuItem: MenuItem

    // Provides location updates for while-in-use feature.
    private var foregroundOnlyLocationService: ForegroundOnlyLocationService? = null

    // Listens for location broadcasts from ForegroundOnlyLocationService.
    private lateinit var foregroundOnlyBroadcastReceiver: ForegroundOnlyBroadcastReceiver

    private var foregroundOnlyLocationServiceBound = false

    // Monitors connection to the while-in-use service.
    private val foregroundOnlyServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as ForegroundOnlyLocationService.LocalBinder
            foregroundOnlyLocationService = binder.service
            foregroundOnlyLocationServiceBound = true
            initLocation()
        }

        override fun onServiceDisconnected(name: ComponentName) {
            foregroundOnlyLocationService = null
            foregroundOnlyLocationServiceBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SharedPreferencesUtils.init(this.applicationContext)
        CoreCtrl.mainActivity = this
        CoreCtrl.initialize()
        // Obtain the FirebaseAnalytics instance.
        CoreCtrl.firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        // Create ForegroundOnlyBroadcastReceiver
        foregroundOnlyBroadcastReceiver = ForegroundOnlyBroadcastReceiver()
        // Initialize ConsentCtrl
        ConsentCtrl.initialize()
        // Initlaize AdmobCtrl
        AdmobCtrl.initialize()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName = getString(R.string.default_notification_channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(
                NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_LOW)
            )
        }

        // Handle possible data accompanying notification message.
        // [START handle_data_extras]
        intent.extras?.let {
            // notification type 0 is a new message
            val notificationType: Any? = it.get("notificationType")
            if (notificationType == IntentConstants.NOTIFICATION_TYPE_MESSAGE || notificationType == IntentConstants.NOTIFICATION_TYPE_HIT) {
                msgDeeplinkId = intent.extras?.get("id")
            }
        }

        FirebaseDynamicLinks.getInstance()
            .getDynamicLink(intent)
            .addOnSuccessListener(this
            ) { pendingDynamicLinkData ->
                // Get deep link from result (may be null if no link is found)
                var deepLink: Uri? = null
                var token: String = ""
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData.link
                    if (deepLink != null) {
                        token = deepLink.getQueryParameter("token").toString()
                    }
                }

                if (deepLink != null && token.isNotEmpty()) {
                    CoreCtrl.handleUI(CoreEvents.PW_FORGET, token)
                } else {
                    startPermission()
                }
            }
            .addOnFailureListener(this
            ) {
                Log.i(TAG, "addOnFailureListener")
            }
            .addOnCanceledListener {
                Log.i(TAG, "addOnCanceldListener")
            }

    }

    fun startPermission() {
        PermissionCtrl.init(this.applicationContext, this)
    }

    fun startApplication() {
        CoreCtrl.handleUI(CoreEvents.START, null)
    }

    fun initLocation() {
        foregroundOnlyLocationService?.subscribeToLocationUpdates()
            ?: Log.d(TAG, "Service Not Bound")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            PermissionCtrl.PERMISSION_CAM_AND_GALLERY ->
                if (grantResults.isNotEmpty()) {
                    val granted: Boolean = grantResults.all { it == PackageManager.PERMISSION_GRANTED }
                    if (granted) {
                        // start application
                        startApplication()
                    } else {
                        PermissionCtrl.showPermissionDialog(this)
                        // build dialog
                    }
                }
        }
    }

    override fun onStart() {
        super.onStart()
        val serviceIntent = Intent(this, ForegroundOnlyLocationService::class.java)
        bindService(serviceIntent, foregroundOnlyServiceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            foregroundOnlyBroadcastReceiver,
            IntentFilter(
                ForegroundOnlyLocationService.ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST)
        )
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            foregroundOnlyBroadcastReceiver
        )
        super.onPause()
    }

    override fun onStop() {
        if (foregroundOnlyLocationServiceBound) {
            unbindService(foregroundOnlyServiceConnection)
            foregroundOnlyLocationServiceBound = false
        }
        super.onStop()
    }

    fun initMainUI(data:Any?) {
        setContentView(R.layout.activity_main)

        indicatorViewModel = ViewModelProvider(this).get(IndicatorViewModel::class.java)

        val apiResponse = data as OnboardingApiResponse

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

        NavigationHeaderUI(apiResponse)

        navController = findNavController(R.id.nav_host_fragment) as NavHostController
        navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_next_hits, R.id.nav_hits, R.id.nav_messages, R.id.nav_profileImages, R.id.nav_filter), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        messageMenuItem = navView.menu[2]
        hitsMenuItem = navView.menu[1]

        supportActionBar?.setHomeAsUpIndicator(R.drawable.custom_home)
        supportActionBar?.setHomeButtonEnabled(true)

        // check for unreadMessages
        if (apiResponse.user.unreadMessages > 0) indicatorViewModel.onChange(
            IndicatorModel(IndicatorConstants.SHOW_INDICATOR_MESSAGES, true)
        )
        indicatorViewModel.model.observe(this, androidx.lifecycle.Observer { model ->
            when(model.type) {
                IndicatorConstants.SHOW_INDICATOR_MESSAGES -> {
                    messageMenuItem.setIcon(R.drawable.custom_messages_indicator)
                }
                IndicatorConstants.HIDE_INDICATOR_MESSAGES ->
                    messageMenuItem.setIcon(R.drawable.custom_messages)
                IndicatorConstants.SHOW_INDICATOR_HIT ->
                    hitsMenuItem.setIcon(R.drawable.custom_hit_indicator)
                IndicatorConstants.HIDE_INDICATOR_HIT ->
                    hitsMenuItem.setIcon(R.drawable.custom_hit)
            }
            if (model.showInfo) {
                supportActionBar?.setHomeAsUpIndicator(R.drawable.custom_home_indicator)
                supportActionBar?.setHomeButtonEnabled(true)
            }
        })
        // check for notifcation deeplink
        if (msgDeeplinkId != "") {
            val bundle = bundleOf("id" to msgDeeplinkId)
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_global_nav_messages, bundle)
            msgDeeplinkId = ""
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == IntentConstants.ACTION_PROFILE_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            CoreCtrl.handleUI(CoreEvents.CAMERA_PROFILE_IMAGE, null)
        } else if (requestCode == IntentConstants.ACTION_PROFILE_IMAGE_PICK && resultCode == RESULT_OK) {
            val imageUri = data?.data
            CoreCtrl.handleUI(CoreEvents.GALLERY_PROFILE_IMAGE, imageUri)
        } else if (requestCode == IntentConstants.ACTION_ACCOUNT_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            CoreCtrl.handleUI(CoreEvents.CAMERA_ACCOUNT_IMAGE, null)
        } else if (requestCode == IntentConstants.ACTION_ACCOUNT_IMAGE_PICK && resultCode == RESULT_OK) {
            val imageUri = data?.data
            CoreCtrl.handleUI(CoreEvents.GALLERY_ACCOUNT_IMAGE, imageUri)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_logout -> {
                SharedPreferencesUtils.deleteId()
                SharedPreferencesUtils.deleteToken()
                logout()
                true
            }
            R.id.action_delete_acc -> {
                DeleteAccountCtrl.showDialog(this)
                true
            }
            R.id.action_licences -> {
                LegalCtrl.showLicenceDialog(this)
                true
            }
            R.id.action_ad_mob -> {
                ConsentCtrl.showConsentForm(false)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun logout() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(
            "finish",
            true
        ) // if you are checking for this in your other Activities

        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or
                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * Receiver for location broadcasts from [ForegroundOnlyLocationService].
     */
    private inner class ForegroundOnlyBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val location = intent.getParcelableExtra<Location>(
                ForegroundOnlyLocationService.EXTRA_LOCATION
            )

            if (location != null) {
                val coder = Geocoder(context, Locale.getDefault())
                val addresses: List<Address>  = coder.getFromLocation(location.latitude, location.longitude, 1)

                Log.d(TAG, "Foreground location: $addresses")
            }
        }
    }
}
