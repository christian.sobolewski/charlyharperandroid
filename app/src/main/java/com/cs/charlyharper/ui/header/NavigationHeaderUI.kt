package com.cs.charlyharper.ui.header

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.request.onboarding.OnboardingApiResponse
import com.cs.charlyharper.utils.KeyboardUtils
import com.google.android.material.navigation.NavigationView

class NavigationHeaderUI(apiResponse: OnboardingApiResponse) {

    private var navView: NavigationView = CoreCtrl.mainActivity.findViewById(R.id.nav_view)
    private var username: TextView = navView.getHeaderView(0).findViewById(R.id.userName)
    private var bio: TextView = navView.getHeaderView(0).findViewById(R.id.bio)

    init {
        updateUI(apiResponse)
        bio.setOnClickListener {
            val builder = AlertDialog.Builder(CoreCtrl.mainActivity).create()
            val dialogView = LayoutInflater.from(CoreCtrl.mainActivity).inflate(R.layout.dialog_bio, null)
            builder.setView(dialogView)

            val cancel: Button = dialogView.findViewById(R.id.btn_cancel)
            val ok: Button = dialogView.findViewById(R.id.btn_ok)
            val dialogTxt = dialogView.findViewById<EditText>(R.id.bio_input)
            val oldText = bio.text
            if (bio.text.isNotEmpty()) {
                dialogTxt.setText(bio.text)
            }
            dialogTxt.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
                if (!focused) {
                    KeyboardUtils.hideKeyboard(view)
                }
            }

            cancel.setOnClickListener{
                builder.dismiss()
            }

            ok.setOnClickListener{
                if (oldText != dialogTxt.text) {
                    val onboardingApiCall = OnboardingApiCall()
                    onboardingApiCall.bioApiCall(dialogTxt.text.toString(), callback = { response ->
                        builder.dismiss()
                        updateUI(response)
                    })
                } else builder.dismiss()
            }

            builder.show()
        }
    }

    private fun updateUI(apiResponse: OnboardingApiResponse) {
        username.text = String.format(CoreCtrl.mainActivity.resources.getString(R.string.user_params), apiResponse.user.username, apiResponse.user.age)
        if (apiResponse.user.bio.isNotEmpty()) {
            bio.text = apiResponse.user.bio
        }
    }


}