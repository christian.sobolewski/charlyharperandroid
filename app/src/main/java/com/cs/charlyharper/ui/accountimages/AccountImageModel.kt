package com.cs.charlyharper.ui.accountimages

import androidx.lifecycle.MutableLiveData

data class AccountImageModel constructor(var liveData: MutableLiveData<AccountImageModel>, var accountImageViewModel : AccountImagesViewModel,
                                         var fileName: String, var slotId:String, var exists:Boolean) {}