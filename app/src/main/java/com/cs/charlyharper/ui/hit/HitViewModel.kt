package com.cs.charlyharper.ui.hit

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.nexthits.NextHitsApiResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HitViewModel : ViewModel() {

    private val _hits = MutableLiveData<List<NextHitsApiResponse>>()
    val hits: LiveData<List<NextHitsApiResponse>> = _hits

    private fun onChange(nextHitsList: List<NextHitsApiResponse>) {
        _hits.postValue(nextHitsList)
    }

    fun onInit() {
        hitsCall()
    }

    @SuppressLint("CheckResult")
    private fun hitsCall() {
        if (UtilMethods.isConnectedToInternet()){
            val observable = ApiService.hitsApiCall().doHits(SharedPreferencesUtils.getID())
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ nextHitsResponse ->
                    onChange(nextHitsResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }
}