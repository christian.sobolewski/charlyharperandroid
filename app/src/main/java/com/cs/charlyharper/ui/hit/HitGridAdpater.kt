package com.cs.charlyharper.ui.hit

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.viewpager2.widget.ViewPager2
import com.cs.charlyharper.R
import com.cs.charlyharper.request.nexthits.NextHitsApiResponse
import com.cs.charlyharper.request.nexthits.NextHitsImage
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class HitGridAdpater constructor(private var rootFragment: HitFragment): BaseAdapter() {

    private lateinit var modelList :List<NextHitsApiResponse>

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val model:NextHitsApiResponse = modelList[position]
        val context: Context = rootFragment.requireContext()
        val inflater = LayoutInflater.from(context)

        var gridItem: View = inflater.inflate(
            R.layout.fragment_hit_cards,
            parent,
            false
        )

        if (convertView == null) {

            val pagerAdapter = HitCollectionPagerAdapter(rootFragment)
            val viewpager: ViewPager2 = gridItem.findViewById(R.id.hit_view_pager)
            viewpager.adapter = pagerAdapter

            val tabs:TabLayout = gridItem.findViewById(R.id.hit_tablayout)

            TabLayoutMediator(tabs, viewpager,true ) { _, _ ->
            }.attach()

            val user = model.user
            val accountImages:List<NextHitsImage> = user.accountImages
            for ((index, _) in accountImages.withIndex()) {
                val hitObjectFragment = pagerAdapter.createFragment(index) as HitObjectFragment
                hitObjectFragment.onInit(user)
                pagerAdapter.notifyItemChanged(index)
            }

        } else {
            gridItem = convertView
        }

        return gridItem
    }

    override fun getItem(position: Int): Any {
        return modelList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        if (modelList.isNotEmpty()) return modelList.size
        return 0
    }

    fun setData(model: List<NextHitsApiResponse>) {
       modelList = model
    }

}