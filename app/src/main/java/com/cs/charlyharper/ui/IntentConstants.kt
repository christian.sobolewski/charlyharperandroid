package com.cs.charlyharper.ui

object IntentConstants {
    const val ACTION_PROFILE_IMAGE_CAPTURE: Int = 0
    const val ACTION_ACCOUNT_IMAGE_CAPTURE: Int = 1

    const val ACTION_PROFILE_IMAGE_PICK: Int = 2
    const val ACTION_ACCOUNT_IMAGE_PICK: Int = 3

    const val ACTION_PROFILE_IMAGE: Int = 4
    const val ACTION_ACCOUNT_IMAGE: Int = 5

    const val NOTIFICATION_TYPE_MESSAGE: String = "0"
    const val NOTIFICATION_TYPE_HIT: String = "1"

}