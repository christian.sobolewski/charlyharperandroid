package com.cs.charlyharper.ui.accountimages

import com.cs.charlyharper.utils.ImageUtils

object AccountImageFactoryDTO {

    private var map = HashMap<String, AccountImageDTO>()

    fun factory(id:String, slotId:String, imageCreatedAt: String, imageUpdatedAt: String): AccountImageDTO {
        return AccountImageDTO(id, slotId, imageCreatedAt, imageUpdatedAt)
    }

    fun factory(id:String, slotId:String, imageCreatedAt: String, imageUpdatedAt: String, store: Boolean): AccountImageDTO {
        if (!map.containsKey(slotId) && store) {
            map[slotId] = factory(id, slotId, imageCreatedAt, imageUpdatedAt)
            map[slotId]!!.imageToLoad = ""
        }
        return map[slotId]!!
    }
}

class AccountImageDTO(var id: String, var slotId: String, var imageCreatedAt: String, var imageUpdatedAt: String) {

    lateinit var imageToLoad: String

    fun isNotEmpty(): Boolean {
        return ImageUtils.getFileName(this).isNotEmpty()
    }

    fun filename() : String {
        return ImageUtils.getFileName(this)
    }
}
