package com.cs.charlyharper.ui.messages

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.messages.MessagesApiResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MessagesViewModel : ViewModel() {

    private val _messages = MutableLiveData<MessagesApiResponse>()
    val messages: LiveData<MessagesApiResponse> = _messages

    private fun onChange(messageResponse: MessagesApiResponse) {
        _messages.postValue(messageResponse)
    }

    fun onInit() {
        messageCall()
    }

    @SuppressLint("CheckResult")
    private fun messageCall() {
        if (UtilMethods.isConnectedToInternet()){
            val observable = ApiService.messagesApiCall().doMessages(SharedPreferencesUtils.getID())
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ messageResponse ->
                    onChange(messageResponse)

                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }
}