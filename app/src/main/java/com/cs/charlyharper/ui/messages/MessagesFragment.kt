package com.cs.charlyharper.ui.messages

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.charlyharper.R
import com.cs.charlyharper.request.messages.MessageApiCalls


class MessagesFragment : Fragment() {

    private val TAG = "MessagesFragment"

    private lateinit var messagesRecyclerView: RecyclerView
    private lateinit var adapter: MessagesAdapter

    private lateinit var messagesViewModel: MessagesViewModel
    private var deeplinkConId: String = ""
    private var currentPosition: Int = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val args = arguments
        if (args != null) deeplinkConId = args.getString("id").toString()

        val root = inflater.inflate(R.layout.fragment_messages, container, false)
        val hint: TextView = root.findViewById(R.id.no_messages_hint)

        val sharedMessagesViewModel = activity?.let { ViewModelProvider(this).get(SharedMessagesViewModel::class.java) }
        sharedMessagesViewModel?.message?.observe(viewLifecycleOwner, Observer { message ->
            val messagesSendApiCall = MessageApiCalls()
            messagesSendApiCall.callMessageSendApi(adapter.getReceiverId(currentPosition), message, callback = { response ->
                //Log.i(TAG, "Update $currentPosition converstion with model $response")
                adapter.updateModelItem(currentPosition, response)
                sharedMessagesViewModel.onChangeConversation(response.conversation.conversations)
                val hint = root.findViewById<TextView>(R.id.no_messages_hint)
                hint.visibility = View.GONE
            })
        })

        sharedMessagesViewModel?.conversationsMark?.observe(viewLifecycleOwner, Observer { response ->
            adapter.updateModelById(response)
        })

        adapter = MessagesAdapter(childFragmentManager, this)

        messagesRecyclerView = root.findViewById(R.id.message_recycler_view)
        messagesRecyclerView.itemAnimator = DefaultItemAnimator();
        messagesRecyclerView.adapter = adapter
        messagesRecyclerView.layoutManager = LinearLayoutManager(root.context)

        val swipeHandler = object : SwipeToDeleteCallback(context) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = messagesRecyclerView.adapter as MessagesAdapter
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(messagesRecyclerView)

        val posModel = ViewModelProvider(this).get(SharedAdapterViewModel::class.java)
        posModel.position.observe(viewLifecycleOwner, Observer {position ->
            currentPosition = position
        })

        messagesViewModel = ViewModelProvider(this).get(MessagesViewModel::class.java)
        messagesViewModel.messages.observe(viewLifecycleOwner, Observer {res ->
            res.user.messages.sortByDescending { it.latestMsgTimestamp }
            if (res.user.messages.isEmpty()) hint.visibility = View.VISIBLE else hint.visibility = View.GONE
            adapter.setModel(res.user.messages)
            if (deeplinkConId.isNotEmpty()) {
                adapter.setDeeplink(deeplinkConId)
                if (!adapter.hasConversation(deeplinkConId)) {
                    val messagesSendApiCall = MessageApiCalls()
                    messagesSendApiCall.callMessageSendApi(deeplinkConId, "", callback = { response ->
                        Log.i(TAG, "Add conversation with model $response")
                        adapter.adapterAddModel(response.conversation)
                        adapter.notifyDataSetChanged()
                    })
                } else {
                    adapter.notifyDataSetChanged()
                }

            } else {
                adapter.notifyDataSetChanged()
            }
        })

        messagesViewModel.onInit()

        return root
    }

    fun updateFragment() {
        deeplinkConId = ""
        messagesViewModel.onInit()
    }

}
