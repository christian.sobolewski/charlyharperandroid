package com.cs.charlyharper.ui.onboarding

import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.CoreEvents
import com.cs.charlyharper.core.pwForget.PwForgetCtrl
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.utils.KeyboardUtils


class OnboardingUI {

    private val TAG = "Layout::OnboardingUI"

    fun init(activity: AppCompatActivity) {
        // get reference to register
        val registerBtn = activity.findViewById(R.id.registerAccount) as TextView
        val signBtn = activity.findViewById(R.id.signInButton) as Button
        val forgotPWBtn = activity.findViewById(R.id.forgotPW) as TextView

        val emailInput = activity.findViewById(R.id.emailInput) as EditText
        emailInput.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
           if (!focused) KeyboardUtils.hideKeyboard(view)
        }

        val pwInput = activity.findViewById(R.id.passwordInput) as EditText
        pwInput.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }

        // set on-click listener register
        registerBtn.setOnClickListener {
            KeyboardUtils.hideKeyboard(emailInput)
            KeyboardUtils.hideKeyboard(pwInput)

            CoreCtrl.handleUI(CoreEvents.REGISTER, null)
        }

        // set on-click listener signIn
        signBtn.setOnClickListener {

            KeyboardUtils.hideKeyboard(emailInput)
            KeyboardUtils.hideKeyboard(pwInput)

            val emailValid = InputVerification.isValidEmail(emailInput.text);
            val pwValid = InputVerification.isValidPW(pwInput.text)

            if (emailValid && pwValid) {
                val apiCall = OnboardingApiCall()
                apiCall.loginApiCall(emailInput.text.toString(), pwInput.text.toString())
            }
            else {
                if (!emailValid) Toast.makeText(activity.applicationContext,R.string.email_validation_error,Toast.LENGTH_LONG).show()
                if (!pwValid) Toast.makeText(activity.applicationContext,R.string.pw_validation_error,Toast.LENGTH_LONG).show()
            }
        }

        // set on-click listener forgotPWBtn
        forgotPWBtn.setOnClickListener {
            PwForgetCtrl.showDialog(activity as MainActivity)
        }
    }
}