package com.cs.charlyharper.ui.accountimages

import android.annotation.SuppressLint
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AccountImageDelete {
    private val TAG = "ProfileImageDelete"

    @SuppressLint("CheckResult")
    fun callProfileImageDelete(slotId:String, callback: (Boolean) -> Unit) {
        if (UtilMethods.isConnectedToInternet()) {
            val observable = ApiService.accountImageApiCall().doDeleteAccountImage(SharedPreferencesUtils.getID()!!, slotId)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ deleteAccountImageResponse ->
                    callback(deleteAccountImageResponse.success)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

}