package com.cs.charlyharper.ui.indicator

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class IndicatorViewModel: ViewModel() {

    private val _model = MutableLiveData<IndicatorModel>()
    val model: LiveData<IndicatorModel> = _model

    fun onChange(mod : IndicatorModel) {
        _model.value = mod
    }
}