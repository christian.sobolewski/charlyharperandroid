package com.cs.charlyharper.ui.messages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedAdapterViewModel : ViewModel() {

    private val _pos = MutableLiveData<Int>()
    val position: LiveData<Int> = _pos

    fun onChange(pos: Int) {
        _pos.value = pos
    }

}