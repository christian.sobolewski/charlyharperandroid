package com.cs.charlyharper.ui.messages

import android.app.AlertDialog
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.request.messages.MessageApiCalls

object MessagesActionCtrl {

    private const val TAG: String = "MessagesActionCtrl"

    fun showDialog(context: MainActivity, id: String, title: String, copy: String, type: Int, scCallback: () -> Unit) {
        val builder = AlertDialog.Builder(context).create()
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_permissions, null)
        builder.setView(dialogView)

        val tfHead = dialogView.findViewById<TextView>(R.id.title)
        tfHead.text = title

        val tfMsg = dialogView.findViewById<TextView>(R.id.message)
        tfMsg.text = copy

        val btnOk: Button = dialogView.findViewById(R.id.btn_ok)
        btnOk.setOnClickListener {
            builder.cancel()
            val api = MessageApiCalls()
            when(type) {
                0 -> {
                    api.reportHit(id, callback = {
                        scCallback()
                    })
                }
                1 -> {
                    api.resolveHit(id,  callback = {
                        scCallback()
                    })
                }
            }
        }

        val btnCancel: Button = dialogView.findViewById(R.id.btn_cancel)
        btnCancel.setOnClickListener {
            builder.cancel()
        }

        builder.show()
    }

}