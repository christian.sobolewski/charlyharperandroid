package com.cs.charlyharper.ui.indicator


data class IndicatorModel constructor(var type: Int, var showInfo: Boolean) {}