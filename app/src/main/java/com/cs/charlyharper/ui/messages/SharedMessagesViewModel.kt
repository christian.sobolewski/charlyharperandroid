package com.cs.charlyharper.ui.messages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.charlyharper.request.messages.ConversationsResponse
import com.cs.charlyharper.request.messages.MessageSendApiResponse
import java.util.ArrayList

class SharedMessagesViewModel : ViewModel() {

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message

    private val _conversations = MutableLiveData<ArrayList<ConversationsResponse>>()
    val conversations: LiveData<ArrayList<ConversationsResponse>> = _conversations

    private val _conversationsMark = MutableLiveData<MessageSendApiResponse>()
    val conversationsMark: LiveData<MessageSendApiResponse> = _conversationsMark

    fun onChange(messageResponse: String) {
        _message.value = messageResponse
    }

    fun onChangeConversation(model : ArrayList<ConversationsResponse>) {
        _conversations.value = model
    }

    fun onChangeConversationMark(model : MessageSendApiResponse) {
        _conversationsMark.value = model
    }

}