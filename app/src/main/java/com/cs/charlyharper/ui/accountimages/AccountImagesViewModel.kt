package com.cs.charlyharper.ui.accountimages

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.utils.ImageUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AccountImagesViewModel : ViewModel() {

    private val TAG = "ProfileImagesViewModel"

    private var _model0 = MutableLiveData<AccountImageModel>()
    var model0: LiveData<AccountImageModel> = _model0
    private var _model1 = MutableLiveData<AccountImageModel>()
    var model1: LiveData<AccountImageModel> = _model1
    private var _model2 = MutableLiveData<AccountImageModel>()
    var model2: LiveData<AccountImageModel> = _model2
    private var _model3 = MutableLiveData<AccountImageModel>()
    var model3: LiveData<AccountImageModel> = _model3


    fun onUpdate(model:AccountImageModel, dto: AccountImageDTO) {
        if (ImageUtils.fileExistsFor(dto)) {
            model.liveData.postValue(model)
        } else {
            accountImageCall(model, dto)
        }
    }

    fun onInit(dto: AccountImageDTO) {
        var model : AccountImageModel? = null

        when(dto.slotId) {
            "1" -> model = AccountImageModel(_model0, this, dto.imageToLoad, dto.slotId, false)
            "2" -> model = AccountImageModel(_model1, this, dto.imageToLoad, dto.slotId, false)
            "3" -> model = AccountImageModel(_model2, this, dto.imageToLoad, dto.slotId, false)
            "4" -> model = AccountImageModel(_model3, this, dto.imageToLoad, dto.slotId, false)
        }

        if (ImageUtils.fileExistsFor(dto)) {
            model?.exists = true
            model?.fileName = ImageUtils.getFileName(dto)
            onUpdate(model!!, dto)
        } else {
            if (model != null) {
                accountImageCall(model, dto)
            }
        }
    }

    @SuppressLint("CheckResult")
    private fun accountImageCall(model : AccountImageModel, dto:AccountImageDTO) {
        if(UtilMethods.isConnectedToInternet()){

            val identifier = "${dto.id}_${dto.slotId}"
            val observable = ApiService.accountImageApiCall().doAccountImage(SharedPreferencesUtils.getID(), dto.slotId, identifier)
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ accountImageResponse ->
                    if (accountImageResponse.errorBody() != null) {
                        model.exists = false
                        model.liveData.postValue(model)
                    } else {
                        val accountImageUpdatedAt = accountImageResponse.headers().get("accountImageUpdatedAt")
                        val accountImageCreatedAt = accountImageResponse.headers().get("accountImageCreatedAt")
                        dto.imageCreatedAt = accountImageCreatedAt!!
                        dto.imageUpdatedAt = accountImageUpdatedAt!!
                        ImageUtils.fileExistsFor(dto)
                        accountImageResponse.body()?.byteStream()
                            ?.let { ImageUtils.copyStreamToFile(it, ImageUtils.getResultFile(dto)) {
                                model.exists = true
                                model.fileName = ImageUtils.getFileName(dto)
                                model.liveData.postValue(model)} }
                    }
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }
}
