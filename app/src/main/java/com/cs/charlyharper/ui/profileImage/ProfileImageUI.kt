package com.cs.charlyharper.ui.profileImage

import android.annotation.SuppressLint
import android.widget.ImageView
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.accountImage.AccountImageConstants
import com.cs.charlyharper.request.onboarding.OnboardingApiResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.IntentConstants
import com.cs.charlyharper.ui.accountimages.AccountImageDTO
import com.cs.charlyharper.ui.accountimages.AccountImageFactoryDTO
import com.cs.charlyharper.ui.selectImage.SelectImage
import com.cs.charlyharper.utils.ImageUtils
import com.cs.charlyharper.utils.PicassoUtils
import com.google.android.material.navigation.NavigationView
import com.squareup.picasso.Transformation
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class ProfileImageUI(loginApiResponse: OnboardingApiResponse) {

    private val TAG = "Layout::ProfileImageUI"

    init {

        val profileImage = AccountImageFactoryDTO.factory(SharedPreferencesUtils.getID()!!,AccountImageConstants.ACCOUNT_IMAGE_ID_0, loginApiResponse.user.accountImageCreatedAt, loginApiResponse.user.accountImageUpdatedAt)

        if (ImageUtils.fileExistsFor(profileImage)) {
            initProfileImage(profileImage)
        } else {
            profileImageCall(profileImage)
        }
    }

    @SuppressLint("CheckResult")
    private fun profileImageCall(profileImageDTO : AccountImageDTO){
        if(UtilMethods.isConnectedToInternet()){
            val identifier = "${profileImageDTO.id}_${profileImageDTO.slotId}"
            val observable = ApiService.accountImageApiCall().doAccountImage(SharedPreferencesUtils.getID(),
                AccountImageConstants.ACCOUNT_IMAGE_ID_0, identifier)

            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ profileResponse ->
                    if (profileResponse.errorBody() != null) {
                        profileImageDTO.imageToLoad = ""
                        initProfileImage(profileImageDTO)
                    } else {

                        profileResponse.body()?.byteStream()
                            ?.let { ImageUtils.copyStreamToFile(it, ImageUtils.getResultFile(profileImageDTO)) {initProfileImage(profileImageDTO)} }

                    }
                }, { error ->
                    RequestErrorUtils.processError(error)
            })
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.error_no_internet))
        }
    }

    fun initProfileImage(dto: AccountImageDTO) {
        var navView: NavigationView = CoreCtrl.mainActivity.findViewById(R.id.nav_view)
        val imageView: ImageView =  navView.getHeaderView(0).findViewById(R.id.profileImage)

        if (dto.isNotEmpty()) {
            val radius = CoreCtrl.mainActivity.applicationContext?.resources?.getDimensionPixelSize(R.dimen.corner_radius)!!
            val transformation: Transformation = RoundedCornersTransformation(radius, 5)
            PicassoUtils.loadImage(ImageUtils.getFileName(dto), imageView, transformation, callback = {})
        }

        imageView.setOnClickListener {
            SelectImage.selectImage(CoreCtrl.mainActivity, IntentConstants.ACTION_PROFILE_IMAGE, AccountImageConstants.ACCOUNT_IMAGE_ID_0)
        }
    }

}