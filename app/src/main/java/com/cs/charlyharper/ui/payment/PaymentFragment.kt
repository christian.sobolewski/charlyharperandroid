package com.cs.charlyharper.ui.payment

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.android.billingclient.api.*
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.util.*
import kotlin.collections.ArrayList

class PaymentFragment(private var currentUserDrawable: Drawable?, private var timer: Long, private var deeplinkPage:Int = 0) : Fragment(),
    PurchasesUpdatedListener {

    private val TAG = "PaymentFragment"

    private lateinit var billingClient: BillingClient
    private val skuList = listOf("com.cs.charlyharper.abo_a", "com.cs.charlyharper.abo_b")

    private lateinit var packageAboGrid: GridView
    private lateinit var viewpager: ViewPager2
    private lateinit var tabs: TabLayout
    private var collections: ArrayList<PaymentCollectionData> = ArrayList()
    private val DELAY_MS: Long = 1500 //delay in milliseconds before task is to be executed
    private val PERIOD_MS: Long = 6000
    private var currentPage: Int = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_payment, container, false)

        setupBillingClient()
        createDataModels()

        val paymentCollectionAdapter = PaymentCollectionAdapter(this)
        viewpager = root.findViewById(R.id.pay_view_pager)
        viewpager.adapter = paymentCollectionAdapter
        tabs = root.findViewById(R.id.pay_tablayout)

        TabLayoutMediator(tabs, viewpager,true ) { tab, position ->
        }.attach()

        for ((index, value) in collections.withIndex()) {
            val payObjectFragment = paymentCollectionAdapter.createFragment(index) as PaymentObjectFragment
            payObjectFragment.setData(value)
            paymentCollectionAdapter.notifyItemChanged(index)
        }

        if (deeplinkPage != 0) {
            currentPage = deeplinkPage
            viewpager.setCurrentItem(currentPage, true);
        }

        initAutoSwipe()

        packageAboGrid = root.findViewById(R.id.pay_gridview)

        return root
    }

    private fun setupBillingClient() {
        billingClient = BillingClient
            .newBuilder(CoreCtrl.mainActivity)
            .enablePendingPurchases()
            .setListener(this)
            .build()

        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingServiceDisconnected() {
                Log.i(TAG, "BILLING | onBillingServiceDisconnected | DISCONNECTED")
            }

            override fun onBillingSetupFinished(p0: BillingResult) {
                if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
                    if (billingClient.isReady) {
                        val params = SkuDetailsParams
                            .newBuilder()
                            .setSkusList(skuList)
                            .setType(BillingClient.SkuType.SUBS)
                            .build()

                        billingClient.querySkuDetailsAsync(params) { result, skuDetailsList ->
                            if (result.responseCode == BillingClient.BillingResponseCode.OK) {
                                Log.i(TAG, "querySkuDetailsAsync, responseCode: ${result.responseCode}")
                                if (skuDetailsList != null) {
                                    initProductAdapter(skuDetailsList)
                                }
                            } else {
                                Log.i(TAG, "Can't querySkuDetailsAsync, responseCode: ${result.responseCode}")
                            }
                        }
                    }
                    Log.i(TAG, "BILLING | startConnection | RESULT OK")
                } else {
                    Log.i(TAG, "BILLING | startConnection | RESULT: ${p0.responseCode}")
                }
            }
        })

    }

    private fun initProductAdapter(skuDetailsList: List<SkuDetails>) {
        //check the skuDetails are coming or not
        Log.i(TAG, skuDetailsList.toString())
        //Trying to add them into the adapter
        val purchaseAdapter = PaymentGridAdapter(skuDetailsList, this.context) {
            val billingFlowParams = BillingFlowParams
                .newBuilder()
                .setSkuDetails(it)
                .build()
            billingClient.launchBillingFlow(CoreCtrl.mainActivity, billingFlowParams)
        }

        packageAboGrid.adapter = purchaseAdapter
    }

    //if billing is successful, responseCode returns 0, else not successful, details: https://developer.android.com/google/play/billing/billing_reference
    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: MutableList<Purchase>?) {
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            for (purchase in purchases) {
                handlePurchase(purchase)
            }
        } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }
    }

    private fun handlePurchase(purchase: Purchase) {
        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.
        val consumeParams =
            ConsumeParams.newBuilder()
                .setPurchaseToken(purchase.purchaseToken)
                .build()

        billingClient.consumeAsync(consumeParams) { billingResult, outToken ->
            if (billingResult.responseCode ==  BillingClient.BillingResponseCode.OK) {
                // Handle the success of the consume operation.
            }
        }
    }

    private fun initAutoSwipe() {
        val handler = Handler()
        val update = Runnable {
            viewpager.setCurrentItem(currentPage % collections.size, true);
            currentPage++
        }
        val timer = Timer()// This will create a new Thread
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }
        }, DELAY_MS, PERIOD_MS)
    }

    private fun createDataModels() {
        val ic1 = ContextCompat.getDrawable(CoreCtrl.mainActivity, R.drawable.ic_revert);
        val ic2 = ContextCompat.getDrawable(CoreCtrl.mainActivity, R.drawable.ic_not_interested);
        collections.add(PaymentCollectionData(resources.getString(R.string.payment_0_h1), resources.getString(R.string.payment_0_copy), currentUserDrawable, timer))
        collections.add(PaymentCollectionData(resources.getString(R.string.payment_1_h1), resources.getString(R.string.payment_1_copy), ic1))
        collections.add(PaymentCollectionData(resources.getString(R.string.payment_2_h1), resources.getString(R.string.payment_2_copy), ic2))
    }


}