package com.cs.charlyharper.ui.hit

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.ApiWorker
import com.cs.charlyharper.request.downloadInterceptor.ProgressEventBus
import com.cs.charlyharper.request.nexthits.NextHitsResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.ui.accountimages.AccountImageDTO
import com.cs.charlyharper.ui.accountimages.AccountImageFactoryDTO
import com.cs.charlyharper.utils.ImageUtils
import com.cs.charlyharper.utils.PicassoUtils
import com.squareup.picasso.Transformation
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class HitCollectionPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val TAG = "HitFragment"

    private var _indices: HashMap<Int, Fragment>? = HashMap<Int, Fragment>()
    private var indices: HashMap<Int, Fragment>
        get() = _indices ?: throw UninitializedPropertyAccessException("\"indices\" was queried before being initialized")
        set(value) {_indices=value}

    private var length:Int = 0

    override fun getItemCount(): Int {
        return length
    }

    override fun createFragment(position: Int): Fragment {
        if (!indices.containsKey(position)) {
            val fragment = HitObjectFragment()
            fragment.arguments = Bundle().apply {
                putInt(ARG_OBJECT, position)
            }

            length++
            indices[position] = fragment
            return fragment
        }
        return indices[position]!!
    }

    fun destroy() {
        for ((_, value) in indices) {
            val item: HitObjectFragment = value as HitObjectFragment
            item.destroy()
        }
        _indices = null
    }

}

private const val ARG_OBJECT = "object"

// Instances of this class are fragments representing a single
// object in our collection.
class HitObjectFragment : Fragment() {

    private val TAG = "HitFragment"
    private var index:Int = 0

    private var _user: NextHitsResponse? = null
    var user: NextHitsResponse
        get() = _user ?: throw UninitializedPropertyAccessException("\"user\" was queried before being initialized")
        set(value) {_user=value}

    private var _imageView: ImageView? = null
    private var imageView: ImageView
        get() = _imageView ?: throw UninitializedPropertyAccessException("\"imageView\" was queried before being initialized")
        set(value) {_imageView=value}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_hit_collection, container, false)
    }

    fun onInit(user: NextHitsResponse?) {
        this._user = user
    }

    fun destroy() {
        _user = null
        _imageView?.setOnLongClickListener(null)
        _imageView = null
        super.onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        arguments?.takeIf{ it.containsKey(ARG_OBJECT) }?.apply {
            index = getInt(ARG_OBJECT)
        }

        val titleTextView: TextView = view.findViewById(R.id.next_hit_collection_name)
        imageView = view.findViewById(R.id.next_hit_collection_image)
        val progressBar: ProgressBar = view.findViewById(R.id.next_hit_progressbar)

        if (user.id.isEmpty()) {
            // we dont have any user to next hit
            titleTextView.visibility = View.GONE
            imageView.visibility = View.GONE
            progressBar.visibility = View.GONE
        } else {

            // Update the UI
            val title = "${user.username}, ${user.age}"
            if (title.isNotEmpty()) titleTextView.text = title

            if (index != -1 && user.accountImages.isNotEmpty()) {
                val dto = AccountImageFactoryDTO.factory(user.id, index.toString(), user.accountImages[index].accountImageCreatedAt, user.accountImages[index].accountImageUpdatedAt)

                if (ImageUtils.fileExistsFor(dto)) {
                    progressBar.visibility = View.GONE
                    onImage(imageView, ImageUtils.getFileName(dto))
                } else {
                    accountImageCall(dto, imageView, progressBar)
                }
            }

            imageView.setOnClickListener {
                val bundle = bundleOf("id" to user.id)
                view.findNavController().navigate(R.id.action_global_nav_messages, bundle)
            }
        }
    }

    private fun onImage(imageView: ImageView, fileName: String) {
        if (fileName.isNotEmpty()) {
            val transformation: Transformation = RoundedCornersTransformation(40, 5)
            PicassoUtils.loadImage(fileName, imageView, transformation, callback = {})
        }
    }

    @Inject
    var progressEventBus: ProgressEventBus? = ApiWorker.progressEventBus

    @SuppressLint("CheckResult")
    private fun accountImageCall(dto: AccountImageDTO, imageView: ImageView, progressBar: ProgressBar) {
        if (UtilMethods.isConnectedToInternet()) {
            val identifier = "${id}_${dto.slotId}"
            val disposable =                                  // handle unsubscription
                progressEventBus?.observable()
                    ?.subscribe({
                        if(it.downloadIdentifier == identifier) {
                            // Display the progress here
                            progressBar.progress = it.progress
                            //Log.d(TAG,"Download Progress - ${it.progress}")
                        }
                    },{
                        //Log.d(TAG,"ProgressEvent Error ${it.toString()}")
                    })

            val observable = ApiService.accountImageApiCall().doAccountImage(dto.id, dto.slotId, identifier)
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ accountImageResponse ->
                    if (accountImageResponse.errorBody() != null) {
                        //onChange("", slotId, imageView)
                    } else {
                        accountImageResponse.body()?.byteStream()
                            ?.let { ImageUtils.copyStreamToFile(it, ImageUtils.getResultFile(dto)) {onImage(imageView, ImageUtils.getFileName(dto))} }

                    }
                }, { error ->
                    RequestErrorUtils.processError(error)
                },{
                    // Download complete
                    if (disposable != null) {
                        if (!disposable.isDisposed) {
                            disposable.dispose()
                        }
                    }
                    progressBar.visibility = View.GONE
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }
}

annotation class Inject
