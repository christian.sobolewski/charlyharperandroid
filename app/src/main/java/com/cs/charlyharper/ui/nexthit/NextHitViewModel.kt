package com.cs.charlyharper.ui.nexthit

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.nexthits.NextDisHitApiResponse
import com.cs.charlyharper.request.nexthits.NextHitsApiResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import retrofit2.HttpException

class NextHitViewModel : ViewModel() {

    private val TAG = "NextHitViewModel"

    private val _hits = MutableLiveData<List<NextHitsApiResponse>>()
    val hits: LiveData<List<NextHitsApiResponse>> = _hits

    private fun onChange(nextHitsList: List<NextHitsApiResponse>) {
        _hits.postValue(nextHitsList)
    }

    @SuppressLint("CheckResult")
    fun nextHitsCall(callback: () -> Unit) {
        if (UtilMethods.isConnectedToInternet()){
            val observable = ApiService.nextHitsApiCall().doNextHits(SharedPreferencesUtils.getID())
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ nextHitsResponse ->
                    onChange(nextHitsResponse)
                }, { error ->
                    val exception = error as HttpException
                    if (exception.code() == 422) {
                        callback()
                    } else RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }
}