package com.cs.charlyharper.ui.indicator

object IndicatorConstants {
    const val SHOW_INDICATOR_MESSAGES: Int = 0
    const val HIDE_INDICATOR_MESSAGES: Int = 1

    const val SHOW_INDICATOR_HIT: Int = 2
    const val HIDE_INDICATOR_HIT: Int = 3

}