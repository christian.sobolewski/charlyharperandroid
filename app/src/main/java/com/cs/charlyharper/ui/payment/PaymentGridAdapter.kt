package com.cs.charlyharper.ui.payment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.android.billingclient.api.SkuDetails
import com.cs.charlyharper.R

class PaymentGridAdapter constructor(private val aboList: List<SkuDetails>, val context: Context?, private val onProductClicked: (SkuDetails) -> Unit): BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val model = aboList[position]
        var gridItem: View? = null

        gridItem = if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            when (position) {
                0 -> inflater.inflate(R.layout.item_purchase_0, parent, false)
                1 -> inflater.inflate(R.layout.item_purchase_1, parent, false)
                2 -> inflater.inflate(R.layout.item_purchase_2, parent, false)
                else -> inflater.inflate(R.layout.item_purchase_0, parent, false)
            }

        } else convertView

        val tf_duration = gridItem?.findViewById<TextView>(R.id.grid_pay_duration)
        tf_duration!!.text = model.title.subSequence(0,2)

        var isSingle = true
        if (model.title.subSequence(0,1) != "1") isSingle = false

        val tf_period = gridItem?.findViewById<TextView>(R.id.grid_pay_month)
        val tf_save = gridItem?.findViewById<TextView>(R.id.grid_pay_savings)

        if (isSingle) {
            tf_period!!.text = parent?.resources?.getString(R.string.payment_single_period)
            tf_save!!.visibility = View.GONE

        } else tf_period!!.text = parent?.resources?.getString(R.string.payment_multiple_period)

        val tf_price = gridItem?.findViewById<TextView>(R.id.grid_pay_price)
        tf_price!!.text = model.price

        gridItem!!.setOnClickListener{
            onProductClicked(aboList[position])
        }

        return gridItem!!
    }

    override fun getItem(position: Int): Any {
        return aboList[position]
    }

    override fun getItemId(position: Int): Long = 0

    override fun getCount(): Int = aboList.size
}