package com.cs.charlyharper.ui.onboarding

import android.os.Build
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.CoreEvents
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.utils.KeyboardUtils
import java.util.*

class RegisterUI {

    private var gender: Int = 0
    private var age: Int = 0
    private var minAge: Int = 18

    fun init(activity: AppCompatActivity) {
        // get reference to ui elements
        val closeBtn = activity.findViewById(R.id.closeBtn) as ImageView
        val registerBtn = activity.findViewById(R.id.signInButton) as Button
        val emailInput = activity.findViewById(R.id.emailInput) as TextView
        emailInput.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }
        val pwInput = activity.findViewById(R.id.passwordInput) as TextView
        pwInput.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }
        val usernameInput = activity.findViewById(R.id.userName) as TextView
        usernameInput.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }
        val rbMale = activity.findViewById(R.id.rb_male) as RadioButton
        val genderGroup = activity.findViewById(R.id.gender_group) as RadioGroup
        val datePicker = activity.findViewById(R.id.agePicker) as DatePicker

        var ageDatePicker = Date()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            datePicker.setOnDateChangedListener { view, year, monthOfYear, dayOfMonth ->
                ageDatePicker = Date(getAgeFromDatepicker(datePicker))
                age = getAge(ageDatePicker)
            }
        }

        genderGroup.setOnCheckedChangeListener { group, checkedId ->
            val radio:RadioButton = group.findViewById(checkedId)
            gender = if (radio == rbMale) { 0 } else { 1 }
        }

        // set on-click listener closeBtn
        closeBtn.setOnClickListener {
            CoreCtrl.handleUI(CoreEvents.REGISTER_UNDONE, null)
        }

        // set on-click listener signIn
        registerBtn.setOnClickListener {

            val emailValid = InputVerification.isValidEmail(emailInput.text)
            val pwValid = InputVerification.isValidPW(pwInput.text)
            val usernameValid = InputVerification.isValidUsername(usernameInput.text)

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                ageDatePicker = Date(getAgeFromDatepicker(datePicker))
                age = getAge(ageDatePicker)
            }

            val hasMinAge = age - minAge >= 0

            if (emailValid && pwValid && usernameValid && hasMinAge) {
                val apiCall = OnboardingApiCall()
                apiCall.registerApiCall(emailInput.text.toString(), pwInput.text.toString(), usernameInput.text.toString(), gender,  ageDatePicker.time.toString())
            }
            else {
                var errors = ""
                if (!emailValid) errors += CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.email_validation_error) + "\n"
                if (!pwValid) errors += CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.pw_validation_error)+ "\n"
                if (!usernameValid) errors += CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.username_validation_error)+ "\n"
                if (!hasMinAge) errors += CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.age_validation_error)
                UtilMethods.showLongToastError(activity.applicationContext, errors)
            }
        }
    }

    private fun getAge(date: Date): Int {
        val calendar = Calendar.getInstance()
        calendar.time = Date(date.time - Date().time)
        return 1970 - (calendar.get(Calendar.YEAR) + 1)
    }

    private fun getAgeFromDatepicker(datePicker: DatePicker): Long {
        val cal = Calendar.getInstance()
        cal[Calendar.DAY_OF_MONTH] = datePicker.dayOfMonth
        cal[Calendar.MONTH] = datePicker.month
        cal[Calendar.YEAR] = datePicker.year
        return cal.timeInMillis
    }


}