package com.cs.charlyharper.ui.onboarding

import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.CoreEvents
import com.cs.charlyharper.request.onboarding.OnboardingApiCall
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.utils.KeyboardUtils

class PwUI {

    fun init(mainActivity: AppCompatActivity, data: Any?) {

        val pw = mainActivity.findViewById<EditText>(R.id.passwordInput)
        val pwR = mainActivity.findViewById<EditText>(R.id.passwordInputRepeat)
        val send = mainActivity.findViewById<Button>(R.id.pw_send_btn)

        pw.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }

        pwR.onFocusChangeListener = View.OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }

        // set on-click listener register
        send.setOnClickListener {
            if (!InputVerification.isValidPwRepeat(pw.text.toString(), pwR.text.toString())) {
                val msg = mainActivity.resources.getString(R.string.error_pw_repeat)
                UtilMethods.showLongToastError(mainActivity.applicationContext, msg)
            } else {
                val api = OnboardingApiCall()
                api.pwRecoverApiCall(pw.text.toString(), data as String)
            }
        }
    }
}