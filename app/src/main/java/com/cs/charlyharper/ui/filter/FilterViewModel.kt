package com.cs.charlyharper.ui.filter

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.filter.FilterData
import com.cs.charlyharper.request.filter.FilterValues
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FilterViewModel: ViewModel() {

    private var _filter = MutableLiveData<FilterData>()
    var filter: LiveData<FilterData> = _filter

    fun onInit() {
        filterCall()
    }

    private fun onChange(filterResponse: FilterData) {
        _filter.value = filterResponse
    }

    @SuppressLint("CheckResult")
    private fun filterCall() {
        if (UtilMethods.isConnectedToInternet()){
            val observable = ApiService.filterApiCall().doFilter(SharedPreferencesUtils.getID())
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ filterResponse ->
                    onChange(filterResponse)
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }

    @SuppressLint("CheckResult")
    fun saveCall(distanceLimit: Int, showGender: Int, rangeAge: ArrayList<Number>) {
        if (UtilMethods.isConnectedToInternet()){
            val observable = ApiService.filterApiCall().doPostFilter(FilterData(FilterValues(SharedPreferencesUtils.getID(), distanceLimit, showGender, rangeAge)))
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ filterResponse ->
                    onChange(filterResponse)
                    CoreCtrl.filterData = filterResponse.filter
                }, { error ->
                    RequestErrorUtils.processError(error)
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                    R.string.error_no_internet))
        }
    }
}