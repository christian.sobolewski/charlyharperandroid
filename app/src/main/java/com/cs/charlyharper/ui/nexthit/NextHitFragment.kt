package com.cs.charlyharper.ui.nexthit

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.transition.Slide
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.core.admob.AdmobCtrl
import com.cs.charlyharper.request.nexthits.NextHitsApiCall
import com.cs.charlyharper.request.nexthits.NextHitsApiResponse
import com.cs.charlyharper.request.nexthits.NextHitsImage
import com.cs.charlyharper.request.nexthits.NextHitsResponse
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.indicator.IndicatorConstants
import com.cs.charlyharper.ui.indicator.IndicatorModel
import com.cs.charlyharper.ui.indicator.IndicatorViewModel
import com.cs.charlyharper.ui.payment.PaymentFragment
import com.yuyakaido.android.cardstackview.*


class NextHitFragment : Fragment(), CardStackListener {

    private val TAG = "NextHitFragment"

    private var initialized: Boolean = false

    private lateinit var indicatorViewModel: IndicatorViewModel
    private lateinit var nexthitViewModel: NextHitViewModel
    private var nexthitViewModelOberver: Observer<List<NextHitsApiResponse>> = Observer<List<NextHitsApiResponse>> {user ->
        if (user.isEmpty()) {
            showEmptyUser()
        } else {
            for (it in user) {
                Log.i(TAG, "Create Card for ${it.user.username}")
                cardStackAdapter.setUser(it.user)
                cardStackAdapter.notifyDataSetChanged()
            }
           //nexthitViewModel.hits.removeObservers(viewLifecycleOwner)
        }
    }

    private lateinit var manager: CardStackLayoutManager
    private var cardStackAdapter: NextHitCardStackAdapter = NextHitCardStackAdapter()
    private lateinit var cardStackView: CardStackView

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_next_hit, container, false)

        indicatorViewModel = ViewModelProvider(CoreCtrl.mainActivity).get(IndicatorViewModel::class.java)
        manager = CardStackLayoutManager(root.context, this)

        cardStackAdapter.rootFragment = this
        cardStackAdapter.manager = manager
        manager.setVisibleCount(3)
        manager.setTranslationInterval(8.0f)
        manager.setScaleInterval(0.75f)
        manager.setSwipeThreshold(0.3f)
        manager.setMaxDegree(20.0f)
        manager.setOverlayInterpolator(LinearInterpolator())
        manager.setSwipeableMethod(SwipeableMethod.None)

        cardStackView = root.findViewById(R.id.card_stack_view)
        cardStackView.layoutManager = manager
        cardStackView.adapter = cardStackAdapter
        cardStackAdapter.cardStackView = cardStackView

        if (!initialized) {
            nexthitViewModel = ViewModelProvider(this).get(NextHitViewModel::class.java)
            nexthitViewModel.hits.observe(viewLifecycleOwner, nexthitViewModelOberver)
        }

        nexthitViewModel.nextHitsCall(callback = {
            showLocationHint()
        })
        initialized = true
        return root
    }

    fun callNextHits() {
        nexthitViewModel.nextHitsCall(callback = {
            showLocationHint()
        })
    }

    override fun onCardDisappeared(view: View?, position: Int) {
        Log.i(TAG, "onCardDisappered:: $position")
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {}

    override fun onCardSwiped(direction: Direction?) {
        val currentUser = cardStackAdapter.getCurrentUser()
        val nextHitApiCall = NextHitsApiCall()
        if (direction == Direction.Left) {
            nextHitApiCall.callDisHitApi(currentUser.id, callback = { response ->
                if (response.possibleId.isNotEmpty()) {
                    val msg = String.format(CoreCtrl.mainActivity.resources.getString(R.string.possible_hit), cardStackAdapter.getCurrentUser().username)
                    UtilMethods.showLongToast(CoreCtrl.mainActivity, msg)
                }
                cardStackAdapter.setCurrentPosition()
                manager.setSwipeableMethod(SwipeableMethod.None)
                if (SharedPreferencesUtils.getAccountType() == 0) AdmobCtrl.updateCounting()
            })
            Log.i(TAG, "dismissed hit")
        } else if (direction == Direction.Right) {
            nextHitApiCall.callHitApi(currentUser.id, callback = { response ->
                // api send we have a hit
                if (response.hit) {
                   indicatorViewModel.onChange(IndicatorModel(IndicatorConstants.SHOW_INDICATOR_HIT,true))
                }
                // hit was allowed
                if (response.hitAllowed) {
                    cardStackAdapter.setCurrentPosition()
                    manager.setSwipeableMethod(SwipeableMethod.None)
                    if (SharedPreferencesUtils.getAccountType() == 0) AdmobCtrl.updateCounting()
                } else {
                    // hit was not allowed -> show payment fragment
                    cardStackView.rewind()
                    if (childFragmentManager.backStackEntryCount == 0) {
                        openPaymentFrag(cardStackAdapter.getCurrentNextDrawable(), response.timer)
                    }
                }
            })
        }
    }

    private fun openPaymentFrag(currentNextDrawable: Drawable?, timer: Long, page : Int = 0) {
        val paymentFragment = PaymentFragment(currentNextDrawable, timer, page)
        paymentFragment.enterTransition = Slide(Gravity.BOTTOM)

        childFragmentManager.beginTransaction()
            .replace(R.id.payment_container, paymentFragment)
            .addToBackStack("PaymentFragment")
            .commit()
    }

    fun swipeRewound() {
        if (SharedPreferencesUtils.getAccountType() == 0) {
            openPaymentFrag(cardStackAdapter.getCurrentNextDrawable(), 0, 1)
        } else {
            manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
            cardStackView.rewind()
            manager.setSwipeableMethod(SwipeableMethod.None)
            cardStackAdapter.rewoundCurrentPosition()
        }
    }

    override fun onCardCanceled() {
        this.manager.setSwipeableMethod(SwipeableMethod.None)
    }

    override fun onCardAppeared(view: View?, position: Int) {
    }

    override fun onCardRewound() {
    }

    private fun showLocationHint() {
        val locHint = context?.resources?.getString(R.string.hint_no_loc)
        showEmptyUser(locHint!!)
    }

    private fun showEmptyUser(locHint: String = "") {
        val list : List<NextHitsImage> = List<NextHitsImage>(size=1) {NextHitsImage("","","", "")}
        cardStackAdapter.setUser(NextHitsResponse("","",-1,-1,"", locHint, null, list))
        cardStackAdapter.notifyDataSetChanged()
    }

}
