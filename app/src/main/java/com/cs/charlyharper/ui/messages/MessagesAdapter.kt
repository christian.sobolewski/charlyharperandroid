package com.cs.charlyharper.ui.messages

import android.annotation.SuppressLint
import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Slide
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.accountImage.AccountImageConstants
import com.cs.charlyharper.request.messages.MessageApiCalls
import com.cs.charlyharper.request.messages.MessageSendApiResponse
import com.cs.charlyharper.request.messages.MessagesResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.accountimages.AccountImageDTO
import com.cs.charlyharper.ui.accountimages.AccountImageFactoryDTO
import com.cs.charlyharper.utils.ImageUtils
import com.cs.charlyharper.utils.PicassoUtils
import com.cs.charlyharper.utils.PresenceUtils
import com.squareup.picasso.Transformation
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation


class MessagesAdapter(private var fragmentManager : FragmentManager, private var lifecycleOwner: ViewModelStoreOwner): RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder>() {

    private val TAG = "MessagesFragment"

    private var model : ArrayList<MessagesResponse> = ArrayList()

    private lateinit var context: Context
    private var deepLinkId: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_messages_recycler_view, parent, false) as View
        return MessagesViewHolder(view, fragmentManager)
    }

    override fun getItemCount(): Int {
        return this.model.size
    }

    fun getReceiverId(position: Int): String {
        if (model.isNotEmpty()) return model[position].id
        return ""
    }

    override fun onBindViewHolder(holder: MessagesViewHolder, position: Int) {
        holder.bind(model[position], lifecycleOwner, model[position].id == deepLinkId)
    }

    fun updateModelItem(position:Int, item: MessageSendApiResponse) {
        updateModel(position, item)
    }

    fun updateModelById(item: MessageSendApiResponse) {
        val searchId = item.conversation.id
        var position: Int = -1
        for ((index, value) in model.withIndex()) {
            if (value.id == searchId) {
                position = index
                break
            }
        }
        if (position != -1) {
            updateModel(position, item)
        }
    }

    private fun updateModel(position: Int, item: MessageSendApiResponse) {
        val updateModel = model[position]
        updateModel.conversations = item.conversation.conversations
        model.removeAt(position)
        model.add(0, updateModel)
        notifyDataSetChanged()
    }

    fun adapterAddModel(conversation: MessagesResponse) {
        model.add(conversation)
    }

    fun setModel(model : ArrayList<MessagesResponse>) {
        this.model = model
    }

    fun removeAt(position: Int) {
        val messagesSendApiCall = MessageApiCalls()
        messagesSendApiCall.callMessageDeleteApi(model[position].id, callback = { response ->
            if (response) {
                val msg = context.resources.getString(
                    R.string.conversation_deleted,
                    model[position].username
                )
                UtilMethods.showLongToast(context, msg)
                model.removeAt(position)
                notifyItemRemoved(position)
            }
        })
    }

    fun hasConversation(conId: String):Boolean {
        return model.any { it.id == conId }
    }

    fun setDeeplink(conId: String) {
        deepLinkId = conId
    }

    class MessagesViewHolder(itemView: View, var fragmentManager: FragmentManager) : RecyclerView.ViewHolder(itemView) {
        private val TAG = "MessagesFragment"
        private val textView:TextView = itemView.findViewById(R.id.item_msg_username)
        private val imageView: ImageView = itemView.findViewById(R.id.item_msg_imageview)
        private val lastMsg: TextView = itemView.findViewById(R.id.item_msg_last)
        private val lastMsgDate: TextView = itemView.findViewById(R.id.item_msg_date)
        private val msgNew: TextView = itemView.findViewById(R.id.item_msg_new)
        private lateinit var messagesChatFragment: MessagesChatFragment
        private lateinit var model: MessagesResponse
        private lateinit var lifecycleOwner: ViewModelStoreOwner
        private lateinit var posModel : SharedAdapterViewModel
        private var hasDeeplink: Boolean = false

        fun bind(mod: MessagesResponse, owner: ViewModelStoreOwner, deeplink: Boolean) {
            model = mod
            lifecycleOwner = owner
            hasDeeplink = deeplink
            textView.text = model.username
            textView.setCompoundDrawablesWithIntrinsicBounds(PresenceUtils.getPresence(model.lastLogin), 0, 0, 0);
            callAccountImage(model)

            posModel = ViewModelProvider(lifecycleOwner).get(SharedAdapterViewModel::class.java)
        }

        private fun openFragment(model: MessagesResponse, lifecycleOwner: ViewModelStoreOwner, posModel: SharedAdapterViewModel) {
            posModel.onChange(this.adapterPosition)

            if (fragmentManager.backStackEntryCount == 0) {
                messagesChatFragment = MessagesChatFragment(model.username, imageView.drawable, model.id, model.conversations, lifecycleOwner)
                messagesChatFragment.enterTransition = Slide(Gravity.BOTTOM);

                fragmentManager.beginTransaction()
                    .replace(R.id.chat_container, messagesChatFragment)
                    .addToBackStack("MessagesChatFragment")
                    .commit()
            }
        }

        private fun callAccountImage(model: MessagesResponse) {
            if (model.conversations.isNotEmpty()) {
                val itsMe = model.conversations[model.conversations.size-1].senderId == SharedPreferencesUtils.getID()
                if (itsMe) {
                    val you = CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.current_user)
                    lastMsg.text = you+" "+model.conversations[model.conversations.size-1].message
                } else lastMsg.text = model.conversations[model.conversations.size-1].message
                lastMsgDate.text = CoreCtrl.getTime(model.conversations[model.conversations.size-1].timestamp.toLong())
                val un = model.conversations.filter { (it.messageRead != null && !it.messageRead) }
                if (un.isNotEmpty()) {
                    msgNew.text = un.size.toString()
                    msgNew.visibility = View.VISIBLE
                } else msgNew.visibility = View.GONE
            } else {
                msgNew.visibility = View.GONE
            }
            val dto = AccountImageFactoryDTO.factory(model.id, AccountImageConstants.ACCOUNT_IMAGE_ID_0, model.accountImageCreatedAt, model.accountImageUpdatedAt)
            if (ImageUtils.fileExistsFor(dto)) {
                onImage(dto, imageView)
            } else {
                profileImageCall(dto)
            }
        }

        private fun onImage(dto: AccountImageDTO, imageView: ImageView) {
            if (dto.isNotEmpty()) {
                val transformation: Transformation = RoundedCornersTransformation(40, 5)
                PicassoUtils.loadImage(dto.filename(), imageView, transformation, callback = {
                    if (hasDeeplink) openFragment(model, lifecycleOwner, posModel)
                    itemView.setOnClickListener(View.OnClickListener {
                        openFragment(model, lifecycleOwner, posModel)
                    })
                })
            }
        }

        @SuppressLint("CheckResult")
        private fun profileImageCall(dto: AccountImageDTO) {
            if(UtilMethods.isConnectedToInternet()){
                val identifier = "${dto.id}_${dto.slotId}"
                val observable = ApiService.accountImageApiCall().doAccountImage(dto.id, AccountImageConstants.ACCOUNT_IMAGE_ID_0,identifier)

                observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ profileResponse ->
                        if (profileResponse.errorBody() != null) {
                        } else {
                            profileResponse.body()?.byteStream()
                                ?.let { ImageUtils.copyStreamToFile(it, ImageUtils.getResultFile(dto)) {onImage(dto, imageView )} }

                        }
                    }, { error ->
                        RequestErrorUtils.processError(error)
                    })
            } else {
                UtilMethods.showLongToastError(CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(R.string.error_no_internet))
            }
        }

    }

}