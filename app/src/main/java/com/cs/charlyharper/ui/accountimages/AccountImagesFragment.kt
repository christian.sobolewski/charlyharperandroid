package com.cs.charlyharper.ui.accountimages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cs.charlyharper.R
import com.cs.charlyharper.request.accountImage.AccountImageConstants
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils

class AccountImagesFragment : Fragment() {

    private val TAG = "GridAdapter"

    private lateinit var accountImagesViewModelSlot0: AccountImagesViewModel

    private lateinit var adapter: AccountImagesGridAdpater
    private lateinit var gridView: GridView

    private var accountImageModels : ArrayList<AccountImageModel> = ArrayList()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root: View = inflater.inflate(R.layout.fragment_profile_images, container, false)

        gridView = root.findViewById(R.id.profile_images_gridview) as GridView
        adapter = AccountImagesGridAdpater(this)

        accountImagesViewModelSlot0 = ViewModelProvider(this).get(AccountImagesViewModel::class.java)

        val id = SharedPreferencesUtils.getID()!!
        val dto0 = AccountImageFactoryDTO.factory(id,AccountImageConstants.ACCOUNT_IMAGE_ID_1,"","",true)
        val dto1 = AccountImageFactoryDTO.factory(id,AccountImageConstants.ACCOUNT_IMAGE_ID_2,"","", true)
        val dto2 = AccountImageFactoryDTO.factory(id,AccountImageConstants.ACCOUNT_IMAGE_ID_3,"","",true)
        val dto3 = AccountImageFactoryDTO.factory(id,AccountImageConstants.ACCOUNT_IMAGE_ID_4,"","", true)

        accountImagesViewModelSlot0.onInit(dto0)
        accountImagesViewModelSlot0.onInit(dto1)
        accountImagesViewModelSlot0.onInit(dto2)
        accountImagesViewModelSlot0.onInit(dto3)

        accountImagesViewModelSlot0.model0.observe(viewLifecycleOwner, Observer { profileImageModel ->
            if (!accountImageModels.contains(profileImageModel)) {
                accountImageModels.add(profileImageModel)
            }
            checkContent()
        })

        accountImagesViewModelSlot0.model1.observe(viewLifecycleOwner, Observer { profileImageModel ->
            if (!accountImageModels.contains(profileImageModel)) {
                accountImageModels.add(profileImageModel)
            }
            checkContent()
        })

        accountImagesViewModelSlot0.model2.observe(viewLifecycleOwner, Observer { profileImageModel ->
            if (!accountImageModels.contains(profileImageModel)) {
                accountImageModels.add(profileImageModel)
            }
            checkContent()
        })

        accountImagesViewModelSlot0.model3.observe(viewLifecycleOwner, Observer { profileImageModel ->
            if (!accountImageModels.contains(profileImageModel)) {
                accountImageModels.add(profileImageModel)
            }
            checkContent()
        })

        return root
    }

    private fun checkContent() {
        if (accountImageModels.size == 4) {
            accountImageModels.sortBy { !it.exists }
            adapter.setData(accountImageModels)
            gridView.adapter = adapter
            adapter.notifyDataSetChanged()
        }
    }
}
