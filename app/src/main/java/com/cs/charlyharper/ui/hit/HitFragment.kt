package com.cs.charlyharper.ui.hit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.ui.indicator.IndicatorConstants
import com.cs.charlyharper.ui.indicator.IndicatorModel
import com.cs.charlyharper.ui.indicator.IndicatorViewModel

class HitFragment : Fragment() {

    private val TAG = "HitFragment"

    private lateinit var indicatorViewModel: IndicatorViewModel

    private lateinit var adapter: HitGridAdpater
    private lateinit var gridView: GridView

    private lateinit var hitViewModel: HitViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_hit, container, false)

        indicatorViewModel = ViewModelProvider(CoreCtrl.mainActivity).get(IndicatorViewModel::class.java)

        gridView = root.findViewById(R.id.hit_gridview) as GridView
        adapter = HitGridAdpater(this)

        hitViewModel = ViewModelProvider(this).get(HitViewModel::class.java)
        hitViewModel.hits.observe(viewLifecycleOwner, Observer {
            indicatorViewModel.onChange(IndicatorModel(IndicatorConstants.HIDE_INDICATOR_HIT,false))
            var hint: TextView = root.findViewById(R.id.hit_no_user)
            if (it.isEmpty()) {
                hint.visibility = View.VISIBLE
            } else {
                hint.visibility = View.GONE
                adapter.setData(it)
                gridView.adapter = adapter
                adapter.notifyDataSetChanged()
            }
        })
        hitViewModel.onInit()
        return root
    }
}
