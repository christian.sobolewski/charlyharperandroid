package com.cs.charlyharper.ui.messages

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.widget.EditText
import android.widget.GridView
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.messages.ConversationsResponse
import com.cs.charlyharper.request.messages.MessageApiCalls
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.ui.indicator.IndicatorConstants
import com.cs.charlyharper.ui.indicator.IndicatorModel
import com.cs.charlyharper.ui.indicator.IndicatorViewModel
import com.cs.charlyharper.utils.KeyboardUtils


class MessagesChatFragment(var username: String, var accImage: Drawable, var conversationId:String, var model: ArrayList<ConversationsResponse>, var lifecycleOwner: ViewModelStoreOwner): Fragment() {

    private val TAG = "MessagesFragment"

    private lateinit var indicatorViewModel: IndicatorViewModel

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        }

        override fun afterTextChanged(editable: Editable) {
        }
    }

    private lateinit var chatAdapter: ChatAdapter
    private lateinit var btnSend:ImageButton
    private lateinit var chatEditText:EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        CoreCtrl.mainActivity.supportActionBar?.hide()

        indicatorViewModel = ViewModelProvider(CoreCtrl.mainActivity).get(IndicatorViewModel::class.java)

        val root = inflater.inflate(R.layout.messages_chat_recycler_view, container, false)
        val chatRecyclerView:RecyclerView = root.findViewById(R.id.chat_recycler_view)

        val sharedMessagesViewModel = activity?.let { ViewModelProvider(lifecycleOwner).get(SharedMessagesViewModel::class.java) }
        sharedMessagesViewModel?.conversations?.observe(viewLifecycleOwner, Observer { response ->
            chatAdapter.updateModelItem(response)
            chatRecyclerView.smoothScrollToPosition(chatAdapter.itemCount)
        })

        chatAdapter = ChatAdapter(model)

        val result = model.filter { (it.messageRead != null && !it.messageRead) }
        if (result.isNotEmpty()) {
            // mark conversations as read
            val messageApiCall = MessageApiCalls()
            messageApiCall.callMarkConversationApi(conversationId, callback = { response ->
                sharedMessagesViewModel?.onChangeConversationMark(response)
                indicatorViewModel.onChange(IndicatorModel(IndicatorConstants.HIDE_INDICATOR_MESSAGES, false))
            })
        }

        chatRecyclerView.adapter = chatAdapter
        chatRecyclerView.layoutManager = LinearLayoutManager(root.context)
        var pos : Int = model.size-1
        if (pos < 0) pos = 0
        chatRecyclerView.smoothScrollToPosition(pos)

        btnSend = root.findViewById(R.id.chat_send_button)
        btnSend.setOnClickListener {
            if (chatEditText.text.isNotEmpty()) {
                sharedMessagesViewModel?.onChange(chatEditText.text.toString())
                chatEditText.text.clear()
                chatEditText.clearFocus()
            }
            else {
                UtilMethods.showLongToast(
                    CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                        R.string.write_message))
            }
        }
        chatEditText = root.findViewById(R.id.chat_editText)
        chatEditText.addTextChangedListener(textWatcher)

        chatEditText.onFocusChangeListener = OnFocusChangeListener { view, focused ->
            if (!focused) KeyboardUtils.hideKeyboard(view)
        }

        val bgA = BarGridAdpater(this)
        bgA.setModel(BarGridModel(username, accImage))

        val gv = root.findViewById<GridView>(R.id.grid_bar)
        gv.adapter = bgA

        bgA.notifyDataSetChanged()

        return root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_report -> {
                val title = CoreCtrl.mainActivity.resources.getString(R.string.action_report_dismiss_title)
                val copy = CoreCtrl.mainActivity.resources.getString(R.string.action_report_dismiss_copy)
                MessagesActionCtrl.showDialog(CoreCtrl.mainActivity, conversationId, title, copy, 0, scCallback = {
                    update()
                })
                true
            }
            R.id.action_dismiss -> {
                val title = CoreCtrl.mainActivity.resources.getString(R.string.action_dismiss_title)
                val copy = CoreCtrl.mainActivity.resources.getString(R.string.action_dismiss_copy)
                MessagesActionCtrl.showDialog(CoreCtrl.mainActivity, conversationId, title, copy, 1, scCallback = {
                    update()
                })
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun update() {
        CoreCtrl.mainActivity.onBackPressed()
        (lifecycleOwner as MessagesFragment).updateFragment()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        CoreCtrl.mainActivity.supportActionBar?.show()
    }

}