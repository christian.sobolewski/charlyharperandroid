package com.cs.charlyharper.ui.nexthit

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.cs.charlyharper.R
import com.cs.charlyharper.request.nexthits.NextHitsImage
import com.cs.charlyharper.request.nexthits.NextHitsResponse
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.yuyakaido.android.cardstackview.CardStackLayoutManager
import com.yuyakaido.android.cardstackview.CardStackView

class NextHitCardStackAdapter : RecyclerView.Adapter<NextHitCardStackAdapter.ViewHolder>() {

    private var currentPostion: Int = 0
    private var users : ArrayList<NextHitsResponse> = ArrayList()
    private var currentUsers:HashMap<Int, NextHitObjectFragment> = HashMap()

    lateinit var rootFragment : Fragment
    lateinit var manager : CardStackLayoutManager
    lateinit var cardStackView : CardStackView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.fragment_next_hit_cards, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]

        val pagerAdapter = NextHitCollectionPagerAdapter(rootFragment, position)
        holder.viewpager.adapter = pagerAdapter

        TabLayoutMediator(holder.tabs, holder.viewpager,true ) { _, _ ->
        }.attach()

        val accountImages:List<NextHitsImage> = user.accountImages
        for ((index, _) in accountImages.withIndex()) {
            val nextHitObjectFragment = pagerAdapter.createFragment(index) as NextHitObjectFragment
            if (index == 0) currentUsers[position] = nextHitObjectFragment
            nextHitObjectFragment.onInit(user, manager, cardStackView)
            pagerAdapter.notifyItemChanged(index)
        }
    }

    override fun getItemCount(): Int {
        return users.size
    }

    fun setUser(item: NextHitsResponse) {
        if (!users.contains(item)) users.add(item)
    }

    fun getCurrentUser():NextHitsResponse {
        return users[currentPostion]
    }

    fun getCurrentNextDrawable(): Drawable? {
        if (currentUsers.containsKey(currentPostion)) {
            return currentUsers[currentPostion]?.getImageDrawable()
        }
        return null
    }

    fun setCurrentPosition() {
        currentPostion++
        if (currentPostion == itemCount) {
            currentPostion = 0
            users.clear()
            (rootFragment as NextHitFragment).callNextHits()
        }
    }

    fun rewoundCurrentPosition() {
        currentPostion--
        if (currentPostion < 0) currentPostion = 0
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val viewpager: ViewPager2 = view.findViewById(R.id.next_hit_view_pager)
        val tabs: TabLayout = view.findViewById(R.id.next_hit_tablayout)

        init {
            //viewpager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            //})
        }
    }
}