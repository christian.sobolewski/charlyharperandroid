package com.cs.charlyharper.ui.payment

import android.graphics.drawable.Drawable

data class PaymentCollectionData(var h1:String, var copy:String, var drawable:Drawable?, var timer: Long = 0) {
}