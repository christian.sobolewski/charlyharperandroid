package com.cs.charlyharper.ui.selectImage

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.widget.Button
import androidx.core.content.FileProvider
import com.cs.charlyharper.MainActivity
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.ui.IntentConstants
import com.cs.charlyharper.ui.accountimages.AccountImageModel
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object SelectImage {

    private lateinit var currentPhotoPath: String
    lateinit var photoURI: Uri
    private var imageAction:Int = 0
    var slotQuery: String = ""

    private var _accountImagesModel: AccountImageModel? = null
    var accountImagesModel: AccountImageModel
        get() = _accountImagesModel ?: throw UninitializedPropertyAccessException("\"profileImagesModel\" was queried before being initialized")
        set(value) {_accountImagesModel=value}

    private lateinit var builder: AlertDialog

    fun selectImage(context: MainActivity, action: Int, slotQuery: String = "", imagesViewModel: AccountImageModel? = null) {
        this.slotQuery = slotQuery
        this._accountImagesModel = imagesViewModel
        builder = AlertDialog.Builder(context).create()
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_select_image, null)
        builder.setView(dialogView)

        val btnTakePhoto: Button = dialogView.findViewById(R.id.btn_take_photo)
        btnTakePhoto.setOnClickListener {
            takeCameraImage(action)
        }

        val btnGallery: Button = dialogView.findViewById(R.id.btn_gallery_photo)
        btnGallery.setOnClickListener {
            chooseImageFromGallery(action)
        }

        val btnCancel: Button = dialogView.findViewById(R.id.btn_cancel)
        btnCancel.setOnClickListener {
            builder.dismiss()
        }
        builder.show()
    }

    fun hasProfileImagesModel(): Boolean {
        if (_accountImagesModel == null) return false
        return true
    }

    private fun takeCameraImage (action: Int) {
        when (action) {
            IntentConstants.ACTION_PROFILE_IMAGE -> imageAction = IntentConstants.ACTION_PROFILE_IMAGE_CAPTURE
            IntentConstants.ACTION_ACCOUNT_IMAGE -> imageAction = IntentConstants.ACTION_ACCOUNT_IMAGE_CAPTURE
        }

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(CoreCtrl.mainActivity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                } as File
                // Continue only if the File was successfully created
                photoFile?.also {
                    photoURI = FileProvider.getUriForFile(
                        CoreCtrl.mainActivity,
                        "com.cs.charlyharper.fullsizeimage.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    CoreCtrl.mainActivity.startActivityForResult(takePictureIntent, imageAction)
                    if (builder != null) builder.dismiss()
                }
            }
        }
    }

    private fun chooseImageFromGallery(action: Int) {
        when (action) {
            IntentConstants.ACTION_PROFILE_IMAGE -> imageAction = IntentConstants.ACTION_PROFILE_IMAGE_PICK
            IntentConstants.ACTION_ACCOUNT_IMAGE -> imageAction = IntentConstants.ACTION_ACCOUNT_IMAGE_PICK
        }
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI).also { takePictureIntent ->
            takePictureIntent.resolveActivity(CoreCtrl.mainActivity.packageManager)?.also {
                CoreCtrl.mainActivity.startActivityForResult(takePictureIntent, imageAction)
                if (builder != null) builder.dismiss()
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = CoreCtrl.mainActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

}