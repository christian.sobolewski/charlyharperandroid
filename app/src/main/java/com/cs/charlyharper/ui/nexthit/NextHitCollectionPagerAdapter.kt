package com.cs.charlyharper.ui.nexthit

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.ApiService
import com.cs.charlyharper.request.ApiWorker
import com.cs.charlyharper.request.downloadInterceptor.ProgressEventBus
import com.cs.charlyharper.request.nexthits.NextHitsResponse
import com.cs.charlyharper.request.utils.RequestErrorUtils
import com.cs.charlyharper.request.utils.UtilMethods
import com.cs.charlyharper.ui.hit.Inject
import com.cs.charlyharper.utils.BitmapUtils
import com.cs.charlyharper.utils.PresenceUtils
import com.yuyakaido.android.cardstackview.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class NextHitCollectionPagerAdapter(var fragment: Fragment, var cardPosition:Int ) : FragmentStateAdapter(fragment) {

    private var _indices: HashMap<Int, Fragment>? = HashMap()
    var indices: HashMap<Int, Fragment>
        get() = _indices ?: throw UninitializedPropertyAccessException("\"indices\" was queried before being initialized")
        set(value) {_indices=value}

    override fun getItemCount(): Int {
        return indices.size
    }

    override fun createFragment(position: Int): Fragment {
        if (!indices.containsKey(position)) {
            val fragment = NextHitObjectFragment(fragment, cardPosition)
            fragment.arguments = Bundle().apply {
                putInt(ARG_OBJECT, position)
            }
            indices[position] = fragment
            return fragment
        }
        return indices[position]!!
    }

    fun destroy() {
        for ((_, value) in indices) {
            val item: NextHitObjectFragment = value as NextHitObjectFragment
            item.destroy()
        }
        _indices = null
    }

}

private const val ARG_OBJECT = "object"

// Instances of this class are fragments representing a single
// object in our collection.
class NextHitObjectFragment(private var rootFragment:Fragment, private var cardPosition: Int) : Fragment() {

    private val TAG = "NextHitFragment"
    private var index:Int = 0

    private var _cardStackView: CardStackView? = null
    var cardStackView: CardStackView
        get() = _cardStackView ?: throw UninitializedPropertyAccessException("\"cardStackView\" was queried before being initialized")
        set(value) {_cardStackView=value}

    private var _user: NextHitsResponse? = null
    var user: NextHitsResponse
        get() = _user ?: throw UninitializedPropertyAccessException("\"user\" was queried before being initialized")
        set(value) {_user=value}

    private var _manager: CardStackLayoutManager? = null
    private var manager: CardStackLayoutManager
        get() = _manager ?: throw UninitializedPropertyAccessException("\"manager\" was queried before being initialized")
        set(value) {_manager=value}

    private var _imageView: ImageView? = null
    private var imageView: ImageView
        get() = _imageView ?: throw UninitializedPropertyAccessException("\"imageView\" was queried before being initialized")
        set(value) {_imageView=value}

    override fun onDestroyView() {
        super.onDestroyView()
        destroy()
        rootFragment.childFragmentManager.beginTransaction().remove(this).commitAllowingStateLoss();
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_next_hit_collection, container, false)
    }

    fun onInit(user: NextHitsResponse?, manager: CardStackLayoutManager?, cardStackView:CardStackView) {
        this._user = user
        this._manager = manager
        this._cardStackView = cardStackView
    }

    fun destroy() {
        super.onDestroy()
    }

    fun getImageDrawable(): Drawable? {
        return imageView.drawable
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        arguments?.takeIf{ it.containsKey(ARG_OBJECT) }?.apply {
            index = getInt(ARG_OBJECT)
        }

        Log.i(TAG, "NextHitObjectFragment created for ${user.username}")

        val titleTextView: TextView = view.findViewById(R.id.next_hit_collection_name)
        val localityTextView: TextView = view.findViewById(R.id.next_hit_collection_locality)
        val bioTextView: TextView = view.findViewById(R.id.next_hit_collection_bio)
        val noUserTextView: TextView = view.findViewById(R.id.next_hit_no_user)
        imageView = view.findViewById(R.id.next_hit_collection_image)
        val btnLeft: ImageButton = view.findViewById(R.id.next_hit_left_btn)
        val btnRight: ImageButton = view.findViewById(R.id.next_hit_right_btn)
        val progressBar: ProgressBar = view.findViewById(R.id.next_hit_progressbar)

        val revertBtn : ImageButton = view.findViewById(R.id.next_hit_swipe_rewound)
        revertBtn.visibility = View.VISIBLE
        revertBtn.isEnabled = cardPosition > 0
        revertBtn.setOnClickListener{
            (rootFragment as NextHitFragment).swipeRewound()
        }

        if (user.id.isEmpty()) {
            // we dont have any user to next hit
            titleTextView.visibility = View.GONE
            localityTextView.visibility = View.GONE
            bioTextView.visibility = View.GONE
            imageView.visibility = View.GONE
            btnLeft.visibility = View.GONE
            btnRight.visibility = View.GONE
            progressBar.visibility = View.GONE
            revertBtn.visibility = View.GONE
            if (!CoreCtrl.hasAccountImage) {
                val noAccImage = context?.resources?.getString((R.string.hint_no_acc_image))
                noUserTextView.text = noAccImage
            } else if (user.locality.isNotEmpty())  {
                noUserTextView.text = user.locality
            }
            noUserTextView.visibility = View.VISIBLE
        } else {

            // Update the UI
            val title = "${user.username}, ${user.age}"
            val locality = "${user.locality}"
            var bio = ""
            if (!user.bio.isNullOrEmpty()) {
                bio = user.bio
            }
            if (title.isNotEmpty()) {
                titleTextView.text = title
                titleTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, PresenceUtils.getPresence(user.lastLogin), 0);
            }
            if (locality.isNotEmpty()) localityTextView.text = locality
            if (bio.isNotEmpty()) bioTextView.text = bio

            if (index != -1 && user.accountImages.isNotEmpty()) {
                accountImageCall(index.toString(), user.id, imageView, progressBar)
            }

            btnRight.setOnClickListener{
                manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
                val setting = SwipeAnimationSetting.Builder()
                    .setDirection(Direction.Right)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(AccelerateInterpolator())
                    .build()
                manager.setSwipeAnimationSetting(setting)
                cardStackView.swipe()
            }

            btnLeft.setOnClickListener{
                manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
                val setting = SwipeAnimationSetting.Builder()
                    .setDirection(Direction.Left)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(AccelerateInterpolator())
                    .build()
                manager.setSwipeAnimationSetting(setting)
                cardStackView.swipe()
            }

            imageView.setOnLongClickListener {
               manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
               return@setOnLongClickListener true
            }
        }
    }

    private fun onImage(imageBytes: ByteArray, imageView: ImageView) {
        imageView.setImageBitmap(BitmapUtils.decodeSampledBitmapFromBytes(imageBytes, imageView.width, imageView.height))
    }

    @Inject
    var progressEventBus: ProgressEventBus? = ApiWorker.progressEventBus

    @SuppressLint("CheckResult")
    private fun accountImageCall(slotId: String, id: String, imageView: ImageView, progressBar: ProgressBar) {
        if (UtilMethods.isConnectedToInternet()) {
            val identifier = "${id}_${slotId}"
            val disposable =                                  // handle unsubscription
                progressEventBus?.observable()
                    ?.subscribe({
                        if(it.downloadIdentifier == identifier) {
                            // Display the progress here
                            progressBar.progress = it.progress
                            //Log.d(TAG,"Download Progress - ${it.progress}")
                        }
                    },{
                        //Log.d(TAG,"ProgressEvent Error ${it.toString()}")
                    })

            val observable = ApiService.accountImageApiCall().doAccountImage(id, slotId, identifier)
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ accountImageResponse ->
                    if (accountImageResponse.errorBody() != null) {
                        //onChange("", slotId, imageView)
                    } else {
                        val image = accountImageResponse.body()?.bytes()
                        if (image != null) {
                            onImage(image, imageView)
                        }
                    }
                }, { error ->
                    RequestErrorUtils.processError(error)
                },{
                    // Download complete
                    if (disposable != null) {
                        if (!disposable.isDisposed) {
                            disposable.dispose()
                        }
                    }
                    progressBar.visibility = View.GONE
                })
        } else {
            UtilMethods.showLongToastError(
                CoreCtrl.mainActivity.applicationContext, CoreCtrl.mainActivity.applicationContext.resources.getString(
                R.string.error_no_internet))
        }
    }

}
