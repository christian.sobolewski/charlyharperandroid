package com.cs.charlyharper.ui.onboarding

import android.text.TextUtils
import android.util.Patterns.*
import com.cs.charlyharper.R

object InputVerification {

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            return EMAIL_ADDRESS.matcher(target!!).matches()
        }
    }

    fun isValidPW(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else return true
    }

    fun isValidPwRepeat(target: String?, repeat: String?): Boolean {
        if (target != null) {
            if (repeat != null) {
                return if (target.isEmpty() || repeat.isEmpty()) {
                    false
                } else target == repeat
            }
        }
        return false
    }

    fun isValidUsername(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target) && target.toString() != R.string.username.toString()) {
            false
        } else return true
    }
}