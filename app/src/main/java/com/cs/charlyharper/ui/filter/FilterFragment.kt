package com.cs.charlyharper.ui.filter

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl

class FilterFragment : Fragment() {

    private val TAG = "FilterFragment"
    private var gender:Int = 0
    private var distanceLimit:Int = 0
    private var range: ArrayList<Number> = ArrayList(2)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root: View = inflater.inflate(R.layout.fragment_filter, container, false)
        val seekBarDistanceLimit: CrystalSeekbar = root.findViewById(R.id.seekBar_distance_limit)
        val seekbarDistanceValue: TextView = root.findViewById(R.id.value_distance)

        val genderGroup = root.findViewById(R.id.gender_group) as RadioGroup

        val seekBarRangeLimit: CrystalRangeSeekbar = root.findViewById(R.id.seekBar_age_range)
        val seekbarRangeValue: TextView = root.findViewById(R.id.value_range)

        val filterViewModel = ViewModelProvider(this).get(FilterViewModel::class.java)
        filterViewModel.filter.observe(viewLifecycleOwner, Observer { filterModel ->
            Log.i(TAG, "updateUI with $filterModel")
        })

        val saveBtn: Button = root.findViewById(R.id.save_btn)
        saveBtn.setOnClickListener{
            filterViewModel.saveCall(distanceLimit, gender, range)
            saveBtn.isEnabled = false
        }
        saveBtn.isEnabled = false

        val defaultFilter = CoreCtrl.filterData

        if (defaultFilter == null) {
            filterViewModel.onInit()
        } else {
            seekBarDistanceLimit.minStartValue = defaultFilter.distanceLimit!!.toFloat()
            seekBarDistanceLimit.apply()
            seekbarDistanceValue.text = "${defaultFilter.distanceLimit} ${context?.resources?.getString(R.string.distance_unit)}"
            gender = defaultFilter.showGender!!
            if (gender == 1) {
                genderGroup.check(R.id.rb_female)
            }
            range.add(defaultFilter.ageRange[0])
            range.add(defaultFilter.ageRange[1])
            seekbarRangeValue.text = "${defaultFilter.ageRange[0]} - ${defaultFilter.ageRange[1]}"
            seekBarRangeLimit.setMinStartValue(defaultFilter.ageRange[0].toFloat())
            seekBarRangeLimit.setMaxStartValue(defaultFilter.ageRange[1].toFloat())
            seekBarRangeLimit.apply()
        }

        genderGroup.setOnCheckedChangeListener { group, checkedId ->
            val rbMale:RadioButton = group.findViewById(R.id.rb_male)
            val radio: RadioButton = group.findViewById(checkedId)
            gender = if (radio == rbMale) { 0 } else { 1 }
            if (!saveBtn.isEnabled) enableSaveButton(saveBtn)
        }

        // Set a SeekBar change listener
        seekBarDistanceLimit.setOnSeekbarChangeListener { minValue ->
            distanceLimit = minValue.toInt()
            seekbarDistanceValue.text = "$minValue ${context?.resources?.getString(R.string.distance_unit)}"
        }

        seekBarDistanceLimit.setOnSeekbarFinalValueListener { minValue ->
            distanceLimit = minValue.toInt()
            if (!saveBtn.isEnabled) enableSaveButton(saveBtn)
        }

        // Set a SeekBar change listener
        seekBarRangeLimit.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            seekbarRangeValue.text = "${minValue} - ${maxValue}"
        }

        seekBarRangeLimit.setOnRangeSeekbarFinalValueListener { minValue, maxValue ->
            range[0] = minValue
            range[1] = maxValue
            if (!saveBtn.isEnabled) enableSaveButton(saveBtn)
        }

        return root
    }

    private fun enableSaveButton(button: Button) {
        button.isEnabled = true
    }

}
