package com.cs.charlyharper.ui.payment

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


private const val TAG = "PaymentFragment"

class PaymentCollectionAdapter(var fragment: Fragment): FragmentStateAdapter(fragment) {

    private var _indices: HashMap<Int, Fragment>? = HashMap()
    var indices: HashMap<Int, Fragment>
        get() = _indices ?: throw UninitializedPropertyAccessException("\"indices\" was queried before being initialized")
        set(value) {_indices=value}

    override fun getItemCount(): Int {
        return indices.size
    }

    override fun createFragment(position: Int): Fragment {
        if (!indices.containsKey(position)) {
            val fragment = PaymentObjectFragment(fragment, position)
            indices[position] = fragment
            return fragment
        }
        return indices[position]!!
    }
}

// Instances of this class are fragments representing a single
// object in our collection.
class PaymentObjectFragment(var rootFragment:Fragment, var position: Int) : Fragment() {

    private lateinit var data : PaymentCollectionData

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_payment_collection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val imageView: ImageView = view.findViewById(R.id.payment_icon)
        val h1 : TextView = view.findViewById(R.id.payment_h2)
        h1.text = this.data.h1
        val copy : TextView = view.findViewById(R.id.payment_copy)
        copy.text = this.data.copy
        when(position) {
            0 -> view.background = ContextCompat.getDrawable(CoreCtrl.mainActivity, R.drawable.payment_draw_0)
            1 -> view.background = ContextCompat.getDrawable(CoreCtrl.mainActivity, R.drawable.payment_draw_1)
            2 -> view.background = ContextCompat.getDrawable(CoreCtrl.mainActivity, R.drawable.payment_draw_2)
        }
        if (this.data.drawable != null) {
            imageView.setImageDrawable(this.data.drawable)
        }

        if (this.data.timer > 0) {
            startCountdown(view)
        }
        else {
            val tf = view.findViewById<TextView>(R.id.payment_timer)
            tf.visibility = View.GONE
        }


    }

    fun setData(data : PaymentCollectionData) {
        this.data = data
    }

    private fun startCountdown(view : View) {
        val tf = view.findViewById<TextView>(R.id.payment_timer)
        object : CountDownTimer(this.data.timer, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val hours =
                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
                        .toInt()
                val minutes =
                    (TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(
                        1
                    ).toInt()).toInt()
                val seconds =
                    (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(
                        1
                    ).toInt()).toInt()

                val timeLeftFormatted: String = java.lang.String.format(
                    Locale.getDefault(),
                    "%02d:%02d:%02d",
                    hours,
                    minutes,
                    seconds
                )
                tf.text = timeLeftFormatted
            }

            override fun onFinish() {
                this.cancel()
                //mTextField.setText("done!")
            }
        }.start()
    }

}