package com.cs.charlyharper.ui.messages

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.request.messages.ConversationsResponse
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import java.util.*

class ChatAdapter(private var model: ArrayList<ConversationsResponse>): RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {

    private val TAG = "MessagesFragment"

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_recycler_view, parent, false) as View
        return ChatViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        val chatModel = this.model[position]

        holder.textTimeStamp.text = CoreCtrl.getTime(chatModel.timestamp.toLong())

        if (chatModel.senderId == SharedPreferencesUtils.getID()) {
            holder.updateCurrentUser(context)
        } else {
            holder.updateChatUser(context)
        }
        holder.textMsg.text = chatModel.message
    }

    fun updateModelItem(model: ArrayList<ConversationsResponse>) {
        this.model = model
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return this.model.size
    }

    class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textTimeStamp: TextView = itemView.findViewById(R.id.item_chat_timestamp)
        val textMsg: TextView = itemView.findViewById(R.id.item_chat_msg)

        fun updateCurrentUser(context: Context) {
            textMsg.background = ContextCompat.getDrawable(context, R.drawable.first_button)
            textMsg.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 1)
                addRule(RelativeLayout.BELOW, R.id.item_chat_timestamp)
            }
            textMsg.layoutParams = params
            textMsg.setPadding(32,32,32,32)
        }

        fun updateChatUser(context: Context) {
            textMsg.background = ContextCompat.getDrawable(context, R.drawable.tertiary_button_no_outline)
            textMsg.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
            val params = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                addRule(RelativeLayout.ALIGN_PARENT_LEFT, 1)
                addRule(RelativeLayout.BELOW, R.id.item_chat_timestamp)
            }
            textMsg.layoutParams = params
            textMsg.setPadding(32,32,32,32)
        }
    }
}