package com.cs.charlyharper.ui.accountimages

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.cs.charlyharper.R
import com.cs.charlyharper.core.CoreCtrl
import com.cs.charlyharper.sharedPreferences.SharedPreferencesUtils
import com.cs.charlyharper.ui.IntentConstants
import com.cs.charlyharper.ui.selectImage.SelectImage
import com.cs.charlyharper.utils.ImageUtils
import com.cs.charlyharper.utils.PicassoUtils
import com.squareup.picasso.Transformation
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation

class AccountImagesGridAdpater constructor(private var rootFragment: AccountImagesFragment): BaseAdapter() {

    private val TAG = "GridAdapter"

    private lateinit var accountImageModel : ArrayList<AccountImageModel>

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val profileModel = getItem(position) as AccountImageModel
        val context: Context = rootFragment.requireContext()
        val inflater = LayoutInflater.from(context)

        var gridItem: View

        if (convertView == null) {
            gridItem = inflater.inflate(R.layout.grid_profile_images, parent, false)
            if (profileModel.exists) {
                loadProfileImage(profileModel, gridItem)
            } else initUI(profileModel, gridItem)
        } else {
            gridItem = convertView
        }

        return gridItem
    }

    override fun getItem(position: Int): Any {
        return accountImageModel[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return accountImageModel.size
    }

    fun setData(model: ArrayList<AccountImageModel>) {
       accountImageModel = model
    }

    private fun loadProfileImage(model: AccountImageModel, gridItem: View) {
        val imageView: ImageView = gridItem.findViewById(R.id.slot)

        val radius = CoreCtrl.mainActivity.applicationContext?.resources?.getDimensionPixelSize(R.dimen.corner_radius)!!
        val transformation: Transformation = RoundedCornersTransformation(radius, 5)
        PicassoUtils.loadImage(model.fileName, imageView, transformation, callback = {})

        initUI(model, gridItem)
    }

    private fun initUI(model: AccountImageModel, view:View) {
        val imageView: ImageView = view.findViewById(R.id.slot)
        val slotId = model.slotId
        imageView.setOnClickListener {
            SelectImage.selectImage(CoreCtrl.mainActivity, IntentConstants.ACTION_ACCOUNT_IMAGE, slotId, model)
        }

        if (model.exists) {
            val plus: ImageView = view.findViewById(R.id.slot_plus_add)
            plus.visibility = View.GONE

            val remove: ImageView = view.findViewById(R.id.slot_plus_remove)
            remove.visibility = View.VISIBLE
            remove.setOnClickListener {
                val profileImageDelete = AccountImageDelete()
                profileImageDelete.callProfileImageDelete(slotId, callback = { success ->
                    if (success) {
                        imageView.setImageDrawable(null);
                        ImageUtils.removeFileFromDevice(model.fileName)

                        plus.visibility = View.VISIBLE
                        remove.visibility = View.GONE
                        Log.i(TAG, "Account Profile image deleted? $success")
                    }
                })
            }
        }
    }
}