package com.cs.charlyharper.ui.messages

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.cs.charlyharper.R

class BarGridAdpater constructor(private var rootFragment: MessagesChatFragment): BaseAdapter() {

    private var modelList :ArrayList<BarGridModel> = ArrayList()

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val model = modelList[position]
        val context: Context = rootFragment.requireContext()
        val inflater = LayoutInflater.from(context)

        var gridItem: View = inflater.inflate(
            R.layout.fragment_messages_bar,
            parent,
            false
        )

        if (convertView == null) {
            val image = gridItem.findViewById<ImageView>(R.id.item_msg_imageview)
            val clone = model.accImage.constantState?.newDrawable()
            image.setImageDrawable(clone)
            val uname = gridItem.findViewById<TextView>(R.id.item_msg_username)
            uname.text = model.username

            val toolbar = gridItem.findViewById<Toolbar>(R.id.item_msg_toolbar)
            toolbar.inflateMenu(R.menu.menu_chat)
            toolbar.setOnMenuItemClickListener {it:MenuItem ->
                rootFragment.onOptionsItemSelected(it)
            }


        } else {
            gridItem = convertView
        }

        return gridItem
    }

    override fun getItem(position: Int): Any {
        return modelList[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return modelList.size
    }

    fun setModel(barGridModel: BarGridModel) {
        if (!modelList.contains(barGridModel)) modelList.add(barGridModel)
    }

}